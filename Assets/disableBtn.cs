﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class disableBtn : MonoBehaviour {

	public Button myButton;
	private int counter = 0;
	public Transform disableImg;

	// Use this for initialization
	void Start () {
		myButton = GetComponent<Button> ();
	}

	public void changeButton()
	{
		counter++;
		if (counter % 2 == 0) {
			disableImg.gameObject.SetActive (false);
		} else {
			disableImg.gameObject.SetActive (true);
		}
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class gpsScript : MonoBehaviour {

	public RawImage img;

	private bool setOriginalValues = true;

	public float originalLatitude;
	public float originalLongitude;
	public float currentLongitude;
	public float currentLatitude;

	string url;

	public float lat;
	public float lon;

	LocationInfo li;

	public int zoom = 18;
	public int mapWidth =640;
	public int mapHeight = 640;

	public enum mapType {roadmap,satellite,hybrid,terrain}
	public mapType mapSelected;
	public int scale;

	public Text latitude;
	public Text longitide;

	IEnumerator GetCoordinates()
	{
		//while true so this function keeps running once started.
		while (true) {
			// check if user has location service enabled
			if (!Input.location.isEnabledByUser)
				yield break;

			// Start service before querying location
			Input.location.Start (1f,.1f);

			// Wait until service initializes
			int maxWait = 20;
			while (Input.location.status == LocationServiceStatus.Initializing && maxWait > 0) {
				yield return new WaitForSeconds (1);
				maxWait--;
			}

			// Service didn't initialize in 20 seconds
			if (maxWait < 1) {
				print ("Timed out");
				yield break;
			}

			// Connection has failed
			if (Input.location.status == LocationServiceStatus.Failed) {
				print ("Unable to determine device location");
				yield break;
			} else {
				// Access granted and location value could be retrieved
				print ("Location: " + Input.location.lastData.latitude + " " + Input.location.lastData.longitude + " " + Input.location.lastData.altitude + " " + Input.location.lastData.horizontalAccuracy + " " + Input.location.lastData.timestamp);

				//if original value has not yet been set save coordinates of player on app start
				if (setOriginalValues) {
					originalLatitude = Input.location.lastData.latitude;
					originalLongitude = Input.location.lastData.longitude;
					setOriginalValues = false;
				}

				//overwrite current lat and lon everytime
				currentLatitude = Input.location.lastData.latitude;
				currentLongitude = Input.location.lastData.longitude;

			}
			Input.location.Stop();
		}
	}

	IEnumerator Map()
	{
		url = "https://maps.googleapis.com/maps/api/staticmap?center=" + currentLatitude + "," + currentLongitude +
			"&zoom=" + zoom + "&size=" + mapWidth + "x" + mapHeight + "&scale=" + scale 
			+"&maptype=" + mapSelected +
			"&markers=color:blue%7Clabel:S%7C40.702147,-74.015794&markers=color:green%7Clabel:G%7C40.711614,-74.012318&markers=color:red%7Clabel:C%7C40.718217,-73.998284&key=AIzaSyByxMEP8C-nBEFSE7AvzO5V4pXQveiCLCo";
		WWW www = new WWW (url);
		yield return www;
		img.texture = www.texture;
		img.SetNativeSize ();

	}

	void Start(){
		//start GetCoordinate() function 
		img = gameObject.GetComponent<RawImage> ();
		StartCoroutine (GetCoordinates ());
		StartCoroutine (Map ());
	}

	public void startGoogleMap(){
		lat = currentLatitude;
		lon = currentLongitude;
		zoom = 18;
		StartCoroutine (Map());
	}

	void Update(){
		latitude.text = currentLatitude.ToString();
		longitide.text = currentLongitude.ToString();

	}
}

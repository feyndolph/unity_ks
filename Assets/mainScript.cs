﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class mainScript : MonoBehaviour {

	public AudioSource As;
	public GameObject loginPanel;

	[Header("LoginPanel")]
	public InputField IdInputField;
	public InputField PassInputField;
	public int counter = 0;

	// Use this for initialization
	void Start () {
		As = GetComponent<AudioSource>();
	}

	public void webView(){
		Application.OpenURL ("https://cms1.ks.ac.kr/kor/Main.do");
	}

	public void ChangeScene (string a)
	{
		Application.LoadLevel (a);
	}

	public void backScene(){
		loginPanel.SetActive (false);
	}

	public void QuitScene(){
		Application.Quit ();
	}

	public void playMusic(AudioClip other){
		counter++;
		if (counter % 2 == 0) {
			As.PlayOneShot (other, 1);
		} else {
			As.Stop ();
		}
	}

	public void openLoginPanel(){
		loginPanel.SetActive (true);
	}

	public void loginBtn(){
		StartCoroutine (LoginCo ());
	}

	IEnumerator LoginCo(){
		Debug.Log (IdInputField.text);
		Debug.Log (PassInputField.text);

		yield return null;
	}



	// Update is called once per frame
	void Update () {
		
	}
}

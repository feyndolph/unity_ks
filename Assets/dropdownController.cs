﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class dropdownController : MonoBehaviour {
	List<string> names = new List<string>(){"건물을 선택해 주세요", "1.멀티미디어대학-1호관(한성관)", "2.이과대학-2호관(자연관)",
"3.예술대학-3호관(예술관)", "4.예술대학-3호관(콘서트홀)", "5.상경대학-4호관(상학관)",
"6.법정대학-5호관(사회관)", "7.문과대학-6호관(인문관)", "8.공과대학-7호관(제1공학관)",
		"9.공과대학-8호관(제2공학관", "10.약학대학-9호관(약,과학관)", "11.신학대학-10호관(강의동)", "12.신학대학-11호관(신학관)",
"13.멀티미디어대학-12호관(멀티미디어관)", "14.멀티미디어대학-22호관(박물관)",
"15.멀티미디어대학-22호관(문화관)", "16.제1학생회관-23호관", "17.제2학생회관-24호관",
"18.용무관-25호관", "19.멀티미덩정보관,본관-26호관", "20.멀티미디어정보관,별관,예노소극장,소강당-26호관",
"21.중앙도서관-27호관", "22.평생교육원스포츠센터-27호관", "23.기숙사-28호관(제1누리생활관)",
"24.기숙사-29호관(제2누리생활관)", "25.건학기념관-30호관", "26.체육관",
"27.주차장", "28.테니스장", "29.운동장", "30.27호관 입구", "31.정문(차량통제소)", "32.동문(차량통제소)"};

	public Dropdown dropdown;
	public RawImage mainImg;
	public Texture[] ks;

	public void Dropdown_IndexChanged(int index){
		mainImg.texture = ks [index];
	}

	// Use this for initialization
	void Start () {
		PopulateList();
	}

	void PopulateList()
	{
		dropdown.AddOptions (names);
	}

	public void ChangeScene (string a)
	{
		Application.LoadLevel (a);
	}

	// Update is called once per frame
	void Update () {

	}
}

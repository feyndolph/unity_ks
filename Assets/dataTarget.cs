﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Vuforia
{
	public class dataTarget : MonoBehaviour {

		public Transform TextTargetName;
		public Transform TextDescrption;
		public Transform ButtonAction;
		public Transform PanelDescription;

		public AudioSource soundTarget;
		public AudioClip clipTarget;

		// Use this for initialization
		void Start () {
			soundTarget = (AudioSource)gameObject.AddComponent<AudioSource> ();
		}

		// Update is called once per frame
		void Update () {
			StateManager sm = TrackerManager.Instance.GetStateManager ();
			IEnumerable<TrackableBehaviour> tbs = sm.GetActiveTrackableBehaviours ();

			foreach (TrackableBehaviour tb in tbs) {
				string name = tb.TrackableName;
				ImageTarget it = tb.Trackable as ImageTarget;
				Vector2 size = it.GetSize ();

				Debug.Log ("Active image target:" + name + " -size: " + size.x + ", " + size.y);

				TextTargetName.GetComponent<Text> ().text = name;
				ButtonAction.gameObject.SetActive (true);
				TextDescrption.gameObject.SetActive (true);
				PanelDescription.gameObject.SetActive (true);

				if (name == "ks_30") {
					ButtonAction.GetComponent<Button> ().onClick.AddListener (delegate {
						playSound ("sounds/TRUMP SONG");
					});
					TextDescrption.GetComponent<Text> ().text = " 취업진로팀, 학상생담센터, 교수학습개발센터, 보건진료소(315호), 다목적홀(3층) ";
				}

				if (name == "hansung_1") {
					ButtonAction.GetComponent<Button> ().onClick.AddListener (delegate {
						playSound ("sounds/TRUMP SONG");
					});
					TextDescrption.GetComponent<Text> ().text = " 멀티미디어정보관, 본관 ";
				}

				if (name == "jayeon_2") {
					ButtonAction.GetComponent<Button> ().onClick.AddListener (delegate {
						playSound ("sounds/TRUMP SONG");
					});
					TextDescrption.GetComponent<Text> ().text = " 이과대학 - 2호관(자연관) ";
				}

				if (name == "art_3") {
					ButtonAction.GetComponent<Button> ().onClick.AddListener (delegate {
						playSound ("sounds/TRUMP SONG");
					});
					TextDescrption.GetComponent<Text> ().text = " 예술대학 - 3호관(예술관) ";
				}

				if (name == "sanghak_4") {
					ButtonAction.GetComponent<Button> ().onClick.AddListener (delegate {
						playSound ("sounds/TRUMP SONG");
					});
					TextDescrption.GetComponent<Text> ().text = " 상경대학 - 4호관(상학관) ";
				}

				if (name == "social_5") {
					ButtonAction.GetComponent<Button> ().onClick.AddListener (delegate {
						playSound ("sounds/TRUMP SONG");
					});
					TextDescrption.GetComponent<Text> ().text = " 법정대학 - 5호관(사회관) ";
				}

				if (name == "inmun_6") {
					ButtonAction.GetComponent<Button> ().onClick.AddListener (delegate {
						playSound ("sounds/TRUMP SONG");
					});
					TextDescrption.GetComponent<Text> ().text = " 문과대학 - 6호관(인문관) ";
				}

				if (name == "gonghak_7") {
					ButtonAction.GetComponent<Button> ().onClick.AddListener (delegate {
						playSound ("sounds/TRUMP SONG");
					});
					TextDescrption.GetComponent<Text> ().text = " 공과대학 - 7호관(제1공학관) ";
				}

				if (name == "gonghak_8") {
					ButtonAction.GetComponent<Button> ().onClick.AddListener (delegate {
						playSound ("sounds/TRUMP SONG");
					});
					TextDescrption.GetComponent<Text> ().text = " 공과대학 - 8호관(제2공학관) ";
				}

				if (name == "medicine_9") {
					ButtonAction.GetComponent<Button> ().onClick.AddListener (delegate {
						playSound ("sounds/TRUMP SONG");
					});
					TextDescrption.GetComponent<Text> ().text = " 약학대학 - 9호관(약·과학관) ";
				}

				if (name == "ks_10") {
					ButtonAction.GetComponent<Button> ().onClick.AddListener (delegate {
						playSound ("sounds/TRUMP SONG");
					});
					TextDescrption.GetComponent<Text> ().text = " 신학대학 - 10호관(강의동) ";
				}

				if (name == "sanhak_11") {
					ButtonAction.GetComponent<Button> ().onClick.AddListener (delegate {
						playSound ("sounds/TRUMP SONG");
					});
					TextDescrption.GetComponent<Text> ().text = " 신학대학 - 11호관(신학관) ";
				}

				if (name == "multi_12") {
					ButtonAction.GetComponent<Button> ().onClick.AddListener (delegate {
						playSound ("sounds/TRUMP SONG");
					});
					TextDescrption.GetComponent<Text> ().text = " 멀티미디어대학 - 12호관(멀티미디어관) ";
				}

				if (name == "no1student_23") {
					ButtonAction.GetComponent<Button> ().onClick.AddListener (delegate {
						playSound ("sounds/TRUMP SONG");
					});
					TextDescrption.GetComponent<Text> ().text = " 제1학생회관 - 23호관 ";
				}

				if (name == "no2student_24") {
					ButtonAction.GetComponent<Button> ().onClick.AddListener (delegate {
						playSound ("sounds/TRUMP SONG");
					});
					TextDescrption.GetComponent<Text> ().text = " 제2학생회관 - 24호관 ";
				}

				if (name == "main_26") {
					ButtonAction.GetComponent<Button> ().onClick.AddListener (delegate {
						playSound ("sounds/TRUMP SONG");
					});
					TextDescrption.GetComponent<Text> ().text = " 멀티미디어정보관, 본관 - 26호관 ";
				}

				if (name == "library_27") {
					ButtonAction.GetComponent<Button> ().onClick.AddListener (delegate {
						playSound ("sounds/TRUMP SONG");
					});
					TextDescrption.GetComponent<Text> ().text = " 중앙도서관 - 27호관 ";
				}

				if (name == "dormitory_28") {
					ButtonAction.GetComponent<Button> ().onClick.AddListener (delegate {
						playSound ("sounds/TRUMP SONG");
					});
					TextDescrption.GetComponent<Text> ().text = " 기숙사 - 28호관(제1누리생활관) ";
				}

				if (name == "dormitory_29") {
					ButtonAction.GetComponent<Button> ().onClick.AddListener (delegate {
						playSound ("sounds/TRUMP SONG");
					});
					TextDescrption.GetComponent<Text> ().text = " 기숙사 - 29호관(제2누리생활관) ";
				}


			}
		}

		void playSound(string ss){
			clipTarget = (AudioClip)Resources.Load (ss);
			soundTarget.clip = clipTarget;
			soundTarget.loop = false;
			soundTarget.playOnAwake = false;
			soundTarget.Play ();
		}
	}
}

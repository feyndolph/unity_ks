﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.AudioSource
struct AudioSource_t1135106623;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.UI.InputField
struct InputField_t1631627530;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// mainScript
struct  mainScript_t3805801630  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.AudioSource mainScript::As
	AudioSource_t1135106623 * ___As_2;
	// UnityEngine.GameObject mainScript::loginPanel
	GameObject_t1756533147 * ___loginPanel_3;
	// UnityEngine.UI.InputField mainScript::IdInputField
	InputField_t1631627530 * ___IdInputField_4;
	// UnityEngine.UI.InputField mainScript::PassInputField
	InputField_t1631627530 * ___PassInputField_5;
	// System.Int32 mainScript::counter
	int32_t ___counter_6;

public:
	inline static int32_t get_offset_of_As_2() { return static_cast<int32_t>(offsetof(mainScript_t3805801630, ___As_2)); }
	inline AudioSource_t1135106623 * get_As_2() const { return ___As_2; }
	inline AudioSource_t1135106623 ** get_address_of_As_2() { return &___As_2; }
	inline void set_As_2(AudioSource_t1135106623 * value)
	{
		___As_2 = value;
		Il2CppCodeGenWriteBarrier(&___As_2, value);
	}

	inline static int32_t get_offset_of_loginPanel_3() { return static_cast<int32_t>(offsetof(mainScript_t3805801630, ___loginPanel_3)); }
	inline GameObject_t1756533147 * get_loginPanel_3() const { return ___loginPanel_3; }
	inline GameObject_t1756533147 ** get_address_of_loginPanel_3() { return &___loginPanel_3; }
	inline void set_loginPanel_3(GameObject_t1756533147 * value)
	{
		___loginPanel_3 = value;
		Il2CppCodeGenWriteBarrier(&___loginPanel_3, value);
	}

	inline static int32_t get_offset_of_IdInputField_4() { return static_cast<int32_t>(offsetof(mainScript_t3805801630, ___IdInputField_4)); }
	inline InputField_t1631627530 * get_IdInputField_4() const { return ___IdInputField_4; }
	inline InputField_t1631627530 ** get_address_of_IdInputField_4() { return &___IdInputField_4; }
	inline void set_IdInputField_4(InputField_t1631627530 * value)
	{
		___IdInputField_4 = value;
		Il2CppCodeGenWriteBarrier(&___IdInputField_4, value);
	}

	inline static int32_t get_offset_of_PassInputField_5() { return static_cast<int32_t>(offsetof(mainScript_t3805801630, ___PassInputField_5)); }
	inline InputField_t1631627530 * get_PassInputField_5() const { return ___PassInputField_5; }
	inline InputField_t1631627530 ** get_address_of_PassInputField_5() { return &___PassInputField_5; }
	inline void set_PassInputField_5(InputField_t1631627530 * value)
	{
		___PassInputField_5 = value;
		Il2CppCodeGenWriteBarrier(&___PassInputField_5, value);
	}

	inline static int32_t get_offset_of_counter_6() { return static_cast<int32_t>(offsetof(mainScript_t3805801630, ___counter_6)); }
	inline int32_t get_counter_6() const { return ___counter_6; }
	inline int32_t* get_address_of_counter_6() { return &___counter_6; }
	inline void set_counter_6(int32_t value)
	{
		___counter_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif

﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// dropdownController
struct dropdownController_t2436896817;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void dropdownController::.ctor()
extern "C"  void dropdownController__ctor_m2274387928 (dropdownController_t2436896817 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void dropdownController::Dropdown_IndexChanged(System.Int32)
extern "C"  void dropdownController_Dropdown_IndexChanged_m2002217041 (dropdownController_t2436896817 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void dropdownController::Start()
extern "C"  void dropdownController_Start_m83138032 (dropdownController_t2436896817 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void dropdownController::PopulateList()
extern "C"  void dropdownController_PopulateList_m3684459452 (dropdownController_t2436896817 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void dropdownController::ChangeScene(System.String)
extern "C"  void dropdownController_ChangeScene_m3478330050 (dropdownController_t2436896817 * __this, String_t* ___a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void dropdownController::Update()
extern "C"  void dropdownController_Update_m1199931865 (dropdownController_t2436896817 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;

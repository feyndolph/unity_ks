﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Transform
struct Transform_t3275118058;
// UnityEngine.AudioSource
struct AudioSource_t1135106623;
// UnityEngine.AudioClip
struct AudioClip_t1932558630;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.dataTarget
struct  dataTarget_t2601526595  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Transform Vuforia.dataTarget::TextTargetName
	Transform_t3275118058 * ___TextTargetName_2;
	// UnityEngine.Transform Vuforia.dataTarget::TextDescrption
	Transform_t3275118058 * ___TextDescrption_3;
	// UnityEngine.Transform Vuforia.dataTarget::ButtonAction
	Transform_t3275118058 * ___ButtonAction_4;
	// UnityEngine.Transform Vuforia.dataTarget::PanelDescription
	Transform_t3275118058 * ___PanelDescription_5;
	// UnityEngine.AudioSource Vuforia.dataTarget::soundTarget
	AudioSource_t1135106623 * ___soundTarget_6;
	// UnityEngine.AudioClip Vuforia.dataTarget::clipTarget
	AudioClip_t1932558630 * ___clipTarget_7;

public:
	inline static int32_t get_offset_of_TextTargetName_2() { return static_cast<int32_t>(offsetof(dataTarget_t2601526595, ___TextTargetName_2)); }
	inline Transform_t3275118058 * get_TextTargetName_2() const { return ___TextTargetName_2; }
	inline Transform_t3275118058 ** get_address_of_TextTargetName_2() { return &___TextTargetName_2; }
	inline void set_TextTargetName_2(Transform_t3275118058 * value)
	{
		___TextTargetName_2 = value;
		Il2CppCodeGenWriteBarrier(&___TextTargetName_2, value);
	}

	inline static int32_t get_offset_of_TextDescrption_3() { return static_cast<int32_t>(offsetof(dataTarget_t2601526595, ___TextDescrption_3)); }
	inline Transform_t3275118058 * get_TextDescrption_3() const { return ___TextDescrption_3; }
	inline Transform_t3275118058 ** get_address_of_TextDescrption_3() { return &___TextDescrption_3; }
	inline void set_TextDescrption_3(Transform_t3275118058 * value)
	{
		___TextDescrption_3 = value;
		Il2CppCodeGenWriteBarrier(&___TextDescrption_3, value);
	}

	inline static int32_t get_offset_of_ButtonAction_4() { return static_cast<int32_t>(offsetof(dataTarget_t2601526595, ___ButtonAction_4)); }
	inline Transform_t3275118058 * get_ButtonAction_4() const { return ___ButtonAction_4; }
	inline Transform_t3275118058 ** get_address_of_ButtonAction_4() { return &___ButtonAction_4; }
	inline void set_ButtonAction_4(Transform_t3275118058 * value)
	{
		___ButtonAction_4 = value;
		Il2CppCodeGenWriteBarrier(&___ButtonAction_4, value);
	}

	inline static int32_t get_offset_of_PanelDescription_5() { return static_cast<int32_t>(offsetof(dataTarget_t2601526595, ___PanelDescription_5)); }
	inline Transform_t3275118058 * get_PanelDescription_5() const { return ___PanelDescription_5; }
	inline Transform_t3275118058 ** get_address_of_PanelDescription_5() { return &___PanelDescription_5; }
	inline void set_PanelDescription_5(Transform_t3275118058 * value)
	{
		___PanelDescription_5 = value;
		Il2CppCodeGenWriteBarrier(&___PanelDescription_5, value);
	}

	inline static int32_t get_offset_of_soundTarget_6() { return static_cast<int32_t>(offsetof(dataTarget_t2601526595, ___soundTarget_6)); }
	inline AudioSource_t1135106623 * get_soundTarget_6() const { return ___soundTarget_6; }
	inline AudioSource_t1135106623 ** get_address_of_soundTarget_6() { return &___soundTarget_6; }
	inline void set_soundTarget_6(AudioSource_t1135106623 * value)
	{
		___soundTarget_6 = value;
		Il2CppCodeGenWriteBarrier(&___soundTarget_6, value);
	}

	inline static int32_t get_offset_of_clipTarget_7() { return static_cast<int32_t>(offsetof(dataTarget_t2601526595, ___clipTarget_7)); }
	inline AudioClip_t1932558630 * get_clipTarget_7() const { return ___clipTarget_7; }
	inline AudioClip_t1932558630 ** get_address_of_clipTarget_7() { return &___clipTarget_7; }
	inline void set_clipTarget_7(AudioClip_t1932558630 * value)
	{
		___clipTarget_7 = value;
		Il2CppCodeGenWriteBarrier(&___clipTarget_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif

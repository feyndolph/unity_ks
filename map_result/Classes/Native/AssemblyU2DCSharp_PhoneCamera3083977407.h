﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.WebCamTexture
struct WebCamTexture_t1079476942;
// UnityEngine.Texture
struct Texture_t2243626319;
// UnityEngine.UI.RawImage
struct RawImage_t2749640213;
// UnityEngine.UI.AspectRatioFitter
struct AspectRatioFitter_t3114550109;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PhoneCamera
struct  PhoneCamera_t3083977407  : public MonoBehaviour_t1158329972
{
public:
	// System.Boolean PhoneCamera::camAvailable
	bool ___camAvailable_2;
	// UnityEngine.WebCamTexture PhoneCamera::backCam
	WebCamTexture_t1079476942 * ___backCam_3;
	// UnityEngine.Texture PhoneCamera::defaultBackground
	Texture_t2243626319 * ___defaultBackground_4;
	// UnityEngine.UI.RawImage PhoneCamera::background
	RawImage_t2749640213 * ___background_5;
	// UnityEngine.UI.AspectRatioFitter PhoneCamera::fit
	AspectRatioFitter_t3114550109 * ___fit_6;

public:
	inline static int32_t get_offset_of_camAvailable_2() { return static_cast<int32_t>(offsetof(PhoneCamera_t3083977407, ___camAvailable_2)); }
	inline bool get_camAvailable_2() const { return ___camAvailable_2; }
	inline bool* get_address_of_camAvailable_2() { return &___camAvailable_2; }
	inline void set_camAvailable_2(bool value)
	{
		___camAvailable_2 = value;
	}

	inline static int32_t get_offset_of_backCam_3() { return static_cast<int32_t>(offsetof(PhoneCamera_t3083977407, ___backCam_3)); }
	inline WebCamTexture_t1079476942 * get_backCam_3() const { return ___backCam_3; }
	inline WebCamTexture_t1079476942 ** get_address_of_backCam_3() { return &___backCam_3; }
	inline void set_backCam_3(WebCamTexture_t1079476942 * value)
	{
		___backCam_3 = value;
		Il2CppCodeGenWriteBarrier(&___backCam_3, value);
	}

	inline static int32_t get_offset_of_defaultBackground_4() { return static_cast<int32_t>(offsetof(PhoneCamera_t3083977407, ___defaultBackground_4)); }
	inline Texture_t2243626319 * get_defaultBackground_4() const { return ___defaultBackground_4; }
	inline Texture_t2243626319 ** get_address_of_defaultBackground_4() { return &___defaultBackground_4; }
	inline void set_defaultBackground_4(Texture_t2243626319 * value)
	{
		___defaultBackground_4 = value;
		Il2CppCodeGenWriteBarrier(&___defaultBackground_4, value);
	}

	inline static int32_t get_offset_of_background_5() { return static_cast<int32_t>(offsetof(PhoneCamera_t3083977407, ___background_5)); }
	inline RawImage_t2749640213 * get_background_5() const { return ___background_5; }
	inline RawImage_t2749640213 ** get_address_of_background_5() { return &___background_5; }
	inline void set_background_5(RawImage_t2749640213 * value)
	{
		___background_5 = value;
		Il2CppCodeGenWriteBarrier(&___background_5, value);
	}

	inline static int32_t get_offset_of_fit_6() { return static_cast<int32_t>(offsetof(PhoneCamera_t3083977407, ___fit_6)); }
	inline AspectRatioFitter_t3114550109 * get_fit_6() const { return ___fit_6; }
	inline AspectRatioFitter_t3114550109 ** get_address_of_fit_6() { return &___fit_6; }
	inline void set_fit_6(AspectRatioFitter_t3114550109 * value)
	{
		___fit_6 = value;
		Il2CppCodeGenWriteBarrier(&___fit_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif

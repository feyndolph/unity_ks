﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Transform
struct Transform_t3275118058;
// Vuforia.TrackableBehaviour
struct TrackableBehaviour_t1779888572;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.DefaultTrackableEventHandler
struct  DefaultTrackableEventHandler_t1082256726  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Transform Vuforia.DefaultTrackableEventHandler::TextTargetName
	Transform_t3275118058 * ___TextTargetName_2;
	// UnityEngine.Transform Vuforia.DefaultTrackableEventHandler::ButtonAction
	Transform_t3275118058 * ___ButtonAction_3;
	// UnityEngine.Transform Vuforia.DefaultTrackableEventHandler::TextDescription
	Transform_t3275118058 * ___TextDescription_4;
	// UnityEngine.Transform Vuforia.DefaultTrackableEventHandler::PanelDescription
	Transform_t3275118058 * ___PanelDescription_5;
	// Vuforia.TrackableBehaviour Vuforia.DefaultTrackableEventHandler::mTrackableBehaviour
	TrackableBehaviour_t1779888572 * ___mTrackableBehaviour_6;

public:
	inline static int32_t get_offset_of_TextTargetName_2() { return static_cast<int32_t>(offsetof(DefaultTrackableEventHandler_t1082256726, ___TextTargetName_2)); }
	inline Transform_t3275118058 * get_TextTargetName_2() const { return ___TextTargetName_2; }
	inline Transform_t3275118058 ** get_address_of_TextTargetName_2() { return &___TextTargetName_2; }
	inline void set_TextTargetName_2(Transform_t3275118058 * value)
	{
		___TextTargetName_2 = value;
		Il2CppCodeGenWriteBarrier(&___TextTargetName_2, value);
	}

	inline static int32_t get_offset_of_ButtonAction_3() { return static_cast<int32_t>(offsetof(DefaultTrackableEventHandler_t1082256726, ___ButtonAction_3)); }
	inline Transform_t3275118058 * get_ButtonAction_3() const { return ___ButtonAction_3; }
	inline Transform_t3275118058 ** get_address_of_ButtonAction_3() { return &___ButtonAction_3; }
	inline void set_ButtonAction_3(Transform_t3275118058 * value)
	{
		___ButtonAction_3 = value;
		Il2CppCodeGenWriteBarrier(&___ButtonAction_3, value);
	}

	inline static int32_t get_offset_of_TextDescription_4() { return static_cast<int32_t>(offsetof(DefaultTrackableEventHandler_t1082256726, ___TextDescription_4)); }
	inline Transform_t3275118058 * get_TextDescription_4() const { return ___TextDescription_4; }
	inline Transform_t3275118058 ** get_address_of_TextDescription_4() { return &___TextDescription_4; }
	inline void set_TextDescription_4(Transform_t3275118058 * value)
	{
		___TextDescription_4 = value;
		Il2CppCodeGenWriteBarrier(&___TextDescription_4, value);
	}

	inline static int32_t get_offset_of_PanelDescription_5() { return static_cast<int32_t>(offsetof(DefaultTrackableEventHandler_t1082256726, ___PanelDescription_5)); }
	inline Transform_t3275118058 * get_PanelDescription_5() const { return ___PanelDescription_5; }
	inline Transform_t3275118058 ** get_address_of_PanelDescription_5() { return &___PanelDescription_5; }
	inline void set_PanelDescription_5(Transform_t3275118058 * value)
	{
		___PanelDescription_5 = value;
		Il2CppCodeGenWriteBarrier(&___PanelDescription_5, value);
	}

	inline static int32_t get_offset_of_mTrackableBehaviour_6() { return static_cast<int32_t>(offsetof(DefaultTrackableEventHandler_t1082256726, ___mTrackableBehaviour_6)); }
	inline TrackableBehaviour_t1779888572 * get_mTrackableBehaviour_6() const { return ___mTrackableBehaviour_6; }
	inline TrackableBehaviour_t1779888572 ** get_address_of_mTrackableBehaviour_6() { return &___mTrackableBehaviour_6; }
	inline void set_mTrackableBehaviour_6(TrackableBehaviour_t1779888572 * value)
	{
		___mTrackableBehaviour_6 = value;
		Il2CppCodeGenWriteBarrier(&___mTrackableBehaviour_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif

﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.Button
struct Button_t2872111280;
// UnityEngine.Transform
struct Transform_t3275118058;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// disableBtn
struct  disableBtn_t3218920940  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.UI.Button disableBtn::myButton
	Button_t2872111280 * ___myButton_2;
	// System.Int32 disableBtn::counter
	int32_t ___counter_3;
	// UnityEngine.Transform disableBtn::disableImg
	Transform_t3275118058 * ___disableImg_4;

public:
	inline static int32_t get_offset_of_myButton_2() { return static_cast<int32_t>(offsetof(disableBtn_t3218920940, ___myButton_2)); }
	inline Button_t2872111280 * get_myButton_2() const { return ___myButton_2; }
	inline Button_t2872111280 ** get_address_of_myButton_2() { return &___myButton_2; }
	inline void set_myButton_2(Button_t2872111280 * value)
	{
		___myButton_2 = value;
		Il2CppCodeGenWriteBarrier(&___myButton_2, value);
	}

	inline static int32_t get_offset_of_counter_3() { return static_cast<int32_t>(offsetof(disableBtn_t3218920940, ___counter_3)); }
	inline int32_t get_counter_3() const { return ___counter_3; }
	inline int32_t* get_address_of_counter_3() { return &___counter_3; }
	inline void set_counter_3(int32_t value)
	{
		___counter_3 = value;
	}

	inline static int32_t get_offset_of_disableImg_4() { return static_cast<int32_t>(offsetof(disableBtn_t3218920940, ___disableImg_4)); }
	inline Transform_t3275118058 * get_disableImg_4() const { return ___disableImg_4; }
	inline Transform_t3275118058 ** get_address_of_disableImg_4() { return &___disableImg_4; }
	inline void set_disableImg_4(Transform_t3275118058 * value)
	{
		___disableImg_4 = value;
		Il2CppCodeGenWriteBarrier(&___disableImg_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif

﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// disableBtn
struct disableBtn_t3218920940;

#include "codegen/il2cpp-codegen.h"

// System.Void disableBtn::.ctor()
extern "C"  void disableBtn__ctor_m3128710759 (disableBtn_t3218920940 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void disableBtn::Start()
extern "C"  void disableBtn_Start_m496005355 (disableBtn_t3218920940 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void disableBtn::changeButton()
extern "C"  void disableBtn_changeButton_m2928653103 (disableBtn_t3218920940 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void disableBtn::Update()
extern "C"  void disableBtn_Update_m2033822126 (disableBtn_t3218920940 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;

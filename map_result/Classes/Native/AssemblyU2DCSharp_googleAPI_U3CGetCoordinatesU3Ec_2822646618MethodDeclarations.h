﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// googleAPI/<GetCoordinates>c__Iterator0
struct U3CGetCoordinatesU3Ec__Iterator0_t2822646618;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void googleAPI/<GetCoordinates>c__Iterator0::.ctor()
extern "C"  void U3CGetCoordinatesU3Ec__Iterator0__ctor_m3759668749 (U3CGetCoordinatesU3Ec__Iterator0_t2822646618 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean googleAPI/<GetCoordinates>c__Iterator0::MoveNext()
extern "C"  bool U3CGetCoordinatesU3Ec__Iterator0_MoveNext_m2039638243 (U3CGetCoordinatesU3Ec__Iterator0_t2822646618 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object googleAPI/<GetCoordinates>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CGetCoordinatesU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2588485987 (U3CGetCoordinatesU3Ec__Iterator0_t2822646618 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object googleAPI/<GetCoordinates>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CGetCoordinatesU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m3839941211 (U3CGetCoordinatesU3Ec__Iterator0_t2822646618 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void googleAPI/<GetCoordinates>c__Iterator0::Dispose()
extern "C"  void U3CGetCoordinatesU3Ec__Iterator0_Dispose_m4240304452 (U3CGetCoordinatesU3Ec__Iterator0_t2822646618 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void googleAPI/<GetCoordinates>c__Iterator0::Reset()
extern "C"  void U3CGetCoordinatesU3Ec__Iterator0_Reset_m1189917070 (U3CGetCoordinatesU3Ec__Iterator0_t2822646618 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;

﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// mainScript
struct mainScript_t3805801630;
// System.String
struct String_t;
// UnityEngine.AudioClip
struct AudioClip_t1932558630;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "UnityEngine_UnityEngine_AudioClip1932558630.h"

// System.Void mainScript::.ctor()
extern "C"  void mainScript__ctor_m3385207755 (mainScript_t3805801630 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void mainScript::Start()
extern "C"  void mainScript_Start_m3179815295 (mainScript_t3805801630 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void mainScript::webView()
extern "C"  void mainScript_webView_m2678079802 (mainScript_t3805801630 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void mainScript::ChangeScene(System.String)
extern "C"  void mainScript_ChangeScene_m362305809 (mainScript_t3805801630 * __this, String_t* ___a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void mainScript::backScene()
extern "C"  void mainScript_backScene_m301566104 (mainScript_t3805801630 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void mainScript::QuitScene()
extern "C"  void mainScript_QuitScene_m2157680520 (mainScript_t3805801630 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void mainScript::playMusic(UnityEngine.AudioClip)
extern "C"  void mainScript_playMusic_m3359848665 (mainScript_t3805801630 * __this, AudioClip_t1932558630 * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void mainScript::openLoginPanel()
extern "C"  void mainScript_openLoginPanel_m9158212 (mainScript_t3805801630 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void mainScript::loginBtn()
extern "C"  void mainScript_loginBtn_m2613765934 (mainScript_t3805801630 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator mainScript::LoginCo()
extern "C"  Il2CppObject * mainScript_LoginCo_m257820890 (mainScript_t3805801630 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void mainScript::Update()
extern "C"  void mainScript_Update_m1211775628 (mainScript_t3805801630 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;

﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AugmentedScript/<GetCoordinates>c__Iterator0
struct U3CGetCoordinatesU3Ec__Iterator0_t2530575918;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void AugmentedScript/<GetCoordinates>c__Iterator0::.ctor()
extern "C"  void U3CGetCoordinatesU3Ec__Iterator0__ctor_m3574080819 (U3CGetCoordinatesU3Ec__Iterator0_t2530575918 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean AugmentedScript/<GetCoordinates>c__Iterator0::MoveNext()
extern "C"  bool U3CGetCoordinatesU3Ec__Iterator0_MoveNext_m4097341209 (U3CGetCoordinatesU3Ec__Iterator0_t2530575918 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object AugmentedScript/<GetCoordinates>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CGetCoordinatesU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3976467861 (U3CGetCoordinatesU3Ec__Iterator0_t2530575918 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object AugmentedScript/<GetCoordinates>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CGetCoordinatesU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m4243701261 (U3CGetCoordinatesU3Ec__Iterator0_t2530575918 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AugmentedScript/<GetCoordinates>c__Iterator0::Dispose()
extern "C"  void U3CGetCoordinatesU3Ec__Iterator0_Dispose_m314298980 (U3CGetCoordinatesU3Ec__Iterator0_t2530575918 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AugmentedScript/<GetCoordinates>c__Iterator0::Reset()
extern "C"  void U3CGetCoordinatesU3Ec__Iterator0_Reset_m1855863082 (U3CGetCoordinatesU3Ec__Iterator0_t2530575918 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;

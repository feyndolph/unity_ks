﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.List`1<System.String>
struct List_1_t1398341365;
// UnityEngine.UI.Dropdown
struct Dropdown_t1985816271;
// UnityEngine.UI.RawImage
struct RawImage_t2749640213;
// UnityEngine.Texture[]
struct TextureU5BU5D_t2474608790;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// dropdownController
struct  dropdownController_t2436896817  : public MonoBehaviour_t1158329972
{
public:
	// System.Collections.Generic.List`1<System.String> dropdownController::names
	List_1_t1398341365 * ___names_2;
	// UnityEngine.UI.Dropdown dropdownController::dropdown
	Dropdown_t1985816271 * ___dropdown_3;
	// UnityEngine.UI.RawImage dropdownController::mainImg
	RawImage_t2749640213 * ___mainImg_4;
	// UnityEngine.Texture[] dropdownController::ks
	TextureU5BU5D_t2474608790* ___ks_5;

public:
	inline static int32_t get_offset_of_names_2() { return static_cast<int32_t>(offsetof(dropdownController_t2436896817, ___names_2)); }
	inline List_1_t1398341365 * get_names_2() const { return ___names_2; }
	inline List_1_t1398341365 ** get_address_of_names_2() { return &___names_2; }
	inline void set_names_2(List_1_t1398341365 * value)
	{
		___names_2 = value;
		Il2CppCodeGenWriteBarrier(&___names_2, value);
	}

	inline static int32_t get_offset_of_dropdown_3() { return static_cast<int32_t>(offsetof(dropdownController_t2436896817, ___dropdown_3)); }
	inline Dropdown_t1985816271 * get_dropdown_3() const { return ___dropdown_3; }
	inline Dropdown_t1985816271 ** get_address_of_dropdown_3() { return &___dropdown_3; }
	inline void set_dropdown_3(Dropdown_t1985816271 * value)
	{
		___dropdown_3 = value;
		Il2CppCodeGenWriteBarrier(&___dropdown_3, value);
	}

	inline static int32_t get_offset_of_mainImg_4() { return static_cast<int32_t>(offsetof(dropdownController_t2436896817, ___mainImg_4)); }
	inline RawImage_t2749640213 * get_mainImg_4() const { return ___mainImg_4; }
	inline RawImage_t2749640213 ** get_address_of_mainImg_4() { return &___mainImg_4; }
	inline void set_mainImg_4(RawImage_t2749640213 * value)
	{
		___mainImg_4 = value;
		Il2CppCodeGenWriteBarrier(&___mainImg_4, value);
	}

	inline static int32_t get_offset_of_ks_5() { return static_cast<int32_t>(offsetof(dropdownController_t2436896817, ___ks_5)); }
	inline TextureU5BU5D_t2474608790* get_ks_5() const { return ___ks_5; }
	inline TextureU5BU5D_t2474608790** get_address_of_ks_5() { return &___ks_5; }
	inline void set_ks_5(TextureU5BU5D_t2474608790* value)
	{
		___ks_5 = value;
		Il2CppCodeGenWriteBarrier(&___ks_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif

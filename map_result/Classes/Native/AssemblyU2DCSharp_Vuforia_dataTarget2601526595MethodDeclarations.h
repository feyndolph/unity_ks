﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Vuforia.dataTarget
struct dataTarget_t2601526595;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void Vuforia.dataTarget::.ctor()
extern "C"  void dataTarget__ctor_m3138718916 (dataTarget_t2601526595 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.dataTarget::Start()
extern "C"  void dataTarget_Start_m605456188 (dataTarget_t2601526595 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.dataTarget::Update()
extern "C"  void dataTarget_Update_m1359960125 (dataTarget_t2601526595 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.dataTarget::playSound(System.String)
extern "C"  void dataTarget_playSound_m3052352581 (dataTarget_t2601526595 * __this, String_t* ___ss0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.dataTarget::<Update>m__0()
extern "C"  void dataTarget_U3CUpdateU3Em__0_m3510452816 (dataTarget_t2601526595 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;

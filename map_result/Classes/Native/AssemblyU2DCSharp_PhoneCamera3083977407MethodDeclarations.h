﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PhoneCamera
struct PhoneCamera_t3083977407;

#include "codegen/il2cpp-codegen.h"

// System.Void PhoneCamera::.ctor()
extern "C"  void PhoneCamera__ctor_m1836018410 (PhoneCamera_t3083977407 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhoneCamera::Start()
extern "C"  void PhoneCamera_Start_m1753036230 (PhoneCamera_t3083977407 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhoneCamera::Update()
extern "C"  void PhoneCamera_Update_m2689176255 (PhoneCamera_t3083977407 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;

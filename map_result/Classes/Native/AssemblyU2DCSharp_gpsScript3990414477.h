﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.RawImage
struct RawImage_t2749640213;
// System.String
struct String_t;
// UnityEngine.UI.Text
struct Text_t356221433;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_LocationInfo1364725149.h"
#include "AssemblyU2DCSharp_gpsScript_mapType4160084606.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// gpsScript
struct  gpsScript_t3990414477  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.UI.RawImage gpsScript::img
	RawImage_t2749640213 * ___img_2;
	// System.Boolean gpsScript::setOriginalValues
	bool ___setOriginalValues_3;
	// System.Single gpsScript::originalLatitude
	float ___originalLatitude_4;
	// System.Single gpsScript::originalLongitude
	float ___originalLongitude_5;
	// System.Single gpsScript::currentLongitude
	float ___currentLongitude_6;
	// System.Single gpsScript::currentLatitude
	float ___currentLatitude_7;
	// System.String gpsScript::url
	String_t* ___url_8;
	// System.Single gpsScript::lat
	float ___lat_9;
	// System.Single gpsScript::lon
	float ___lon_10;
	// UnityEngine.LocationInfo gpsScript::li
	LocationInfo_t1364725149  ___li_11;
	// System.Int32 gpsScript::zoom
	int32_t ___zoom_12;
	// System.Int32 gpsScript::mapWidth
	int32_t ___mapWidth_13;
	// System.Int32 gpsScript::mapHeight
	int32_t ___mapHeight_14;
	// gpsScript/mapType gpsScript::mapSelected
	int32_t ___mapSelected_15;
	// System.Int32 gpsScript::scale
	int32_t ___scale_16;
	// UnityEngine.UI.Text gpsScript::latitude
	Text_t356221433 * ___latitude_17;
	// UnityEngine.UI.Text gpsScript::longitide
	Text_t356221433 * ___longitide_18;

public:
	inline static int32_t get_offset_of_img_2() { return static_cast<int32_t>(offsetof(gpsScript_t3990414477, ___img_2)); }
	inline RawImage_t2749640213 * get_img_2() const { return ___img_2; }
	inline RawImage_t2749640213 ** get_address_of_img_2() { return &___img_2; }
	inline void set_img_2(RawImage_t2749640213 * value)
	{
		___img_2 = value;
		Il2CppCodeGenWriteBarrier(&___img_2, value);
	}

	inline static int32_t get_offset_of_setOriginalValues_3() { return static_cast<int32_t>(offsetof(gpsScript_t3990414477, ___setOriginalValues_3)); }
	inline bool get_setOriginalValues_3() const { return ___setOriginalValues_3; }
	inline bool* get_address_of_setOriginalValues_3() { return &___setOriginalValues_3; }
	inline void set_setOriginalValues_3(bool value)
	{
		___setOriginalValues_3 = value;
	}

	inline static int32_t get_offset_of_originalLatitude_4() { return static_cast<int32_t>(offsetof(gpsScript_t3990414477, ___originalLatitude_4)); }
	inline float get_originalLatitude_4() const { return ___originalLatitude_4; }
	inline float* get_address_of_originalLatitude_4() { return &___originalLatitude_4; }
	inline void set_originalLatitude_4(float value)
	{
		___originalLatitude_4 = value;
	}

	inline static int32_t get_offset_of_originalLongitude_5() { return static_cast<int32_t>(offsetof(gpsScript_t3990414477, ___originalLongitude_5)); }
	inline float get_originalLongitude_5() const { return ___originalLongitude_5; }
	inline float* get_address_of_originalLongitude_5() { return &___originalLongitude_5; }
	inline void set_originalLongitude_5(float value)
	{
		___originalLongitude_5 = value;
	}

	inline static int32_t get_offset_of_currentLongitude_6() { return static_cast<int32_t>(offsetof(gpsScript_t3990414477, ___currentLongitude_6)); }
	inline float get_currentLongitude_6() const { return ___currentLongitude_6; }
	inline float* get_address_of_currentLongitude_6() { return &___currentLongitude_6; }
	inline void set_currentLongitude_6(float value)
	{
		___currentLongitude_6 = value;
	}

	inline static int32_t get_offset_of_currentLatitude_7() { return static_cast<int32_t>(offsetof(gpsScript_t3990414477, ___currentLatitude_7)); }
	inline float get_currentLatitude_7() const { return ___currentLatitude_7; }
	inline float* get_address_of_currentLatitude_7() { return &___currentLatitude_7; }
	inline void set_currentLatitude_7(float value)
	{
		___currentLatitude_7 = value;
	}

	inline static int32_t get_offset_of_url_8() { return static_cast<int32_t>(offsetof(gpsScript_t3990414477, ___url_8)); }
	inline String_t* get_url_8() const { return ___url_8; }
	inline String_t** get_address_of_url_8() { return &___url_8; }
	inline void set_url_8(String_t* value)
	{
		___url_8 = value;
		Il2CppCodeGenWriteBarrier(&___url_8, value);
	}

	inline static int32_t get_offset_of_lat_9() { return static_cast<int32_t>(offsetof(gpsScript_t3990414477, ___lat_9)); }
	inline float get_lat_9() const { return ___lat_9; }
	inline float* get_address_of_lat_9() { return &___lat_9; }
	inline void set_lat_9(float value)
	{
		___lat_9 = value;
	}

	inline static int32_t get_offset_of_lon_10() { return static_cast<int32_t>(offsetof(gpsScript_t3990414477, ___lon_10)); }
	inline float get_lon_10() const { return ___lon_10; }
	inline float* get_address_of_lon_10() { return &___lon_10; }
	inline void set_lon_10(float value)
	{
		___lon_10 = value;
	}

	inline static int32_t get_offset_of_li_11() { return static_cast<int32_t>(offsetof(gpsScript_t3990414477, ___li_11)); }
	inline LocationInfo_t1364725149  get_li_11() const { return ___li_11; }
	inline LocationInfo_t1364725149 * get_address_of_li_11() { return &___li_11; }
	inline void set_li_11(LocationInfo_t1364725149  value)
	{
		___li_11 = value;
	}

	inline static int32_t get_offset_of_zoom_12() { return static_cast<int32_t>(offsetof(gpsScript_t3990414477, ___zoom_12)); }
	inline int32_t get_zoom_12() const { return ___zoom_12; }
	inline int32_t* get_address_of_zoom_12() { return &___zoom_12; }
	inline void set_zoom_12(int32_t value)
	{
		___zoom_12 = value;
	}

	inline static int32_t get_offset_of_mapWidth_13() { return static_cast<int32_t>(offsetof(gpsScript_t3990414477, ___mapWidth_13)); }
	inline int32_t get_mapWidth_13() const { return ___mapWidth_13; }
	inline int32_t* get_address_of_mapWidth_13() { return &___mapWidth_13; }
	inline void set_mapWidth_13(int32_t value)
	{
		___mapWidth_13 = value;
	}

	inline static int32_t get_offset_of_mapHeight_14() { return static_cast<int32_t>(offsetof(gpsScript_t3990414477, ___mapHeight_14)); }
	inline int32_t get_mapHeight_14() const { return ___mapHeight_14; }
	inline int32_t* get_address_of_mapHeight_14() { return &___mapHeight_14; }
	inline void set_mapHeight_14(int32_t value)
	{
		___mapHeight_14 = value;
	}

	inline static int32_t get_offset_of_mapSelected_15() { return static_cast<int32_t>(offsetof(gpsScript_t3990414477, ___mapSelected_15)); }
	inline int32_t get_mapSelected_15() const { return ___mapSelected_15; }
	inline int32_t* get_address_of_mapSelected_15() { return &___mapSelected_15; }
	inline void set_mapSelected_15(int32_t value)
	{
		___mapSelected_15 = value;
	}

	inline static int32_t get_offset_of_scale_16() { return static_cast<int32_t>(offsetof(gpsScript_t3990414477, ___scale_16)); }
	inline int32_t get_scale_16() const { return ___scale_16; }
	inline int32_t* get_address_of_scale_16() { return &___scale_16; }
	inline void set_scale_16(int32_t value)
	{
		___scale_16 = value;
	}

	inline static int32_t get_offset_of_latitude_17() { return static_cast<int32_t>(offsetof(gpsScript_t3990414477, ___latitude_17)); }
	inline Text_t356221433 * get_latitude_17() const { return ___latitude_17; }
	inline Text_t356221433 ** get_address_of_latitude_17() { return &___latitude_17; }
	inline void set_latitude_17(Text_t356221433 * value)
	{
		___latitude_17 = value;
		Il2CppCodeGenWriteBarrier(&___latitude_17, value);
	}

	inline static int32_t get_offset_of_longitide_18() { return static_cast<int32_t>(offsetof(gpsScript_t3990414477, ___longitide_18)); }
	inline Text_t356221433 * get_longitide_18() const { return ___longitide_18; }
	inline Text_t356221433 ** get_address_of_longitide_18() { return &___longitide_18; }
	inline void set_longitide_18(Text_t356221433 * value)
	{
		___longitide_18 = value;
		Il2CppCodeGenWriteBarrier(&___longitide_18, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif

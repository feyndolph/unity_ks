﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GPS/<StartLocationService>c__Iterator0
struct U3CStartLocationServiceU3Ec__Iterator0_t560764166;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void GPS/<StartLocationService>c__Iterator0::.ctor()
extern "C"  void U3CStartLocationServiceU3Ec__Iterator0__ctor_m1816285207 (U3CStartLocationServiceU3Ec__Iterator0_t560764166 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GPS/<StartLocationService>c__Iterator0::MoveNext()
extern "C"  bool U3CStartLocationServiceU3Ec__Iterator0_MoveNext_m3471417 (U3CStartLocationServiceU3Ec__Iterator0_t560764166 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object GPS/<StartLocationService>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CStartLocationServiceU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1021115445 (U3CStartLocationServiceU3Ec__Iterator0_t560764166 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object GPS/<StartLocationService>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CStartLocationServiceU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m2059420909 (U3CStartLocationServiceU3Ec__Iterator0_t560764166 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GPS/<StartLocationService>c__Iterator0::Dispose()
extern "C"  void U3CStartLocationServiceU3Ec__Iterator0_Dispose_m3711098124 (U3CStartLocationServiceU3Ec__Iterator0_t560764166 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GPS/<StartLocationService>c__Iterator0::Reset()
extern "C"  void U3CStartLocationServiceU3Ec__Iterator0_Reset_m3534451030 (U3CStartLocationServiceU3Ec__Iterator0_t560764166 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;

﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AugmentedScript
struct AugmentedScript_t524146549;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "codegen/il2cpp-codegen.h"

// System.Void AugmentedScript::.ctor()
extern "C"  void AugmentedScript__ctor_m735199924 (AugmentedScript_t524146549 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator AugmentedScript::GetCoordinates()
extern "C"  Il2CppObject * AugmentedScript_GetCoordinates_m2844805741 (AugmentedScript_t524146549 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AugmentedScript::Calc(System.Single,System.Single,System.Single,System.Single)
extern "C"  void AugmentedScript_Calc_m2842079441 (AugmentedScript_t524146549 * __this, float ___lat10, float ___lon11, float ___lat22, float ___lon23, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AugmentedScript::Start()
extern "C"  void AugmentedScript_Start_m2058296444 (AugmentedScript_t524146549 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AugmentedScript::Update()
extern "C"  void AugmentedScript_Update_m329957273 (AugmentedScript_t524146549 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;

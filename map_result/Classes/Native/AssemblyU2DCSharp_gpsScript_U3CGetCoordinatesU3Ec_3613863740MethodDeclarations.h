﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// gpsScript/<GetCoordinates>c__Iterator0
struct U3CGetCoordinatesU3Ec__Iterator0_t3613863740;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void gpsScript/<GetCoordinates>c__Iterator0::.ctor()
extern "C"  void U3CGetCoordinatesU3Ec__Iterator0__ctor_m157648867 (U3CGetCoordinatesU3Ec__Iterator0_t3613863740 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean gpsScript/<GetCoordinates>c__Iterator0::MoveNext()
extern "C"  bool U3CGetCoordinatesU3Ec__Iterator0_MoveNext_m1277166933 (U3CGetCoordinatesU3Ec__Iterator0_t3613863740 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object gpsScript/<GetCoordinates>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CGetCoordinatesU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m4278640609 (U3CGetCoordinatesU3Ec__Iterator0_t3613863740 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object gpsScript/<GetCoordinates>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CGetCoordinatesU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m2246448089 (U3CGetCoordinatesU3Ec__Iterator0_t3613863740 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void gpsScript/<GetCoordinates>c__Iterator0::Dispose()
extern "C"  void U3CGetCoordinatesU3Ec__Iterator0_Dispose_m751183482 (U3CGetCoordinatesU3Ec__Iterator0_t3613863740 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void gpsScript/<GetCoordinates>c__Iterator0::Reset()
extern "C"  void U3CGetCoordinatesU3Ec__Iterator0_Reset_m3619276244 (U3CGetCoordinatesU3Ec__Iterator0_t3613863740 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;

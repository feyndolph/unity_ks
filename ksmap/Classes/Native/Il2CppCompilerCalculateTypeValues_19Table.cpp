﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaNativeWrapp2645113514.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaWrapper3750170617.h"
#include "Vuforia_UnityExtensions_Vuforia_ReconstructionAbst3509595417.h"
#include "Vuforia_UnityExtensions_Vuforia_PropAbstractBehavi1047177596.h"
#include "Vuforia_UnityExtensions_Vuforia_SmartTerrainTracke1462833936.h"
#include "Vuforia_UnityExtensions_Vuforia_StateManager3369465942.h"
#include "Vuforia_UnityExtensions_Vuforia_StateManagerImpl3885489748.h"
#include "Vuforia_UnityExtensions_Vuforia_TargetFinderImpl1380851697.h"
#include "Vuforia_UnityExtensions_Vuforia_TargetFinderImpl_T3807887646.h"
#include "Vuforia_UnityExtensions_Vuforia_TargetFinderImpl_I2369108641.h"
#include "Vuforia_UnityExtensions_Vuforia_TrackableSourceImp2574642394.h"
#include "Vuforia_UnityExtensions_Vuforia_TextureRenderer3312477626.h"
#include "Vuforia_UnityExtensions_Vuforia_TrackableImpl3421455115.h"
#include "Vuforia_UnityExtensions_Vuforia_TrackerManagerImpl381223961.h"
#include "Vuforia_UnityExtensions_Vuforia_VirtualButtonImpl2449737797.h"
#include "Vuforia_UnityExtensions_Vuforia_WebCamImpl2771725761.h"
#include "Vuforia_UnityExtensions_Vuforia_WebCamProfile3757625748.h"
#include "Vuforia_UnityExtensions_Vuforia_WebCamProfile_Prof1724666488.h"
#include "Vuforia_UnityExtensions_Vuforia_WebCamProfile_Prof3644865120.h"
#include "Vuforia_UnityExtensions_Vuforia_Image1391689025.h"
#include "Vuforia_UnityExtensions_Vuforia_Image_PIXEL_FORMAT3010530044.h"
#include "Vuforia_UnityExtensions_Vuforia_ImageTargetAbstrac3327552701.h"
#include "Vuforia_UnityExtensions_Vuforia_ObjectTracker1568044035.h"
#include "Vuforia_UnityExtensions_Vuforia_MaskOutAbstractBeh3489038957.h"
#include "Vuforia_UnityExtensions_Vuforia_MultiTargetAbstrac3616801211.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaUnity657456673.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaUnity_InitE2149396216.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaUnity_Vufor3491240575.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaUnity_Stora3897282321.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaARControlle4061728485.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaARControlle3506117492.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaMacros1884408435.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaManager2424874861.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaManager_Tra1329355276.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaRenderer2933102835.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaRenderer_Fp1598668988.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaRenderer_Vi4106934884.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaRenderer_Vi4137084396.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaRenderer_Vec829768013.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaRenderer_Vi2617831468.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaRenderer_Ren804170727.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaRuntimeUtil3083157244.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaRuntimeUtil1916387570.h"
#include "Vuforia_UnityExtensions_Vuforia_SurfaceUtilities4096327849.h"
#include "Vuforia_UnityExtensions_Vuforia_TargetFinder1347637805.h"
#include "Vuforia_UnityExtensions_Vuforia_TargetFinder_InitStat4409649.h"
#include "Vuforia_UnityExtensions_Vuforia_TargetFinder_Updat1473252352.h"
#include "Vuforia_UnityExtensions_Vuforia_TargetFinder_Filte3082493643.h"
#include "Vuforia_UnityExtensions_Vuforia_TargetFinder_Targe1958726506.h"
#include "Vuforia_UnityExtensions_Vuforia_TextRecoAbstractBe2386081773.h"
#include "Vuforia_UnityExtensions_Vuforia_TextTracker89845299.h"
#include "Vuforia_UnityExtensions_Vuforia_SimpleTargetData3993525265.h"
#include "Vuforia_UnityExtensions_Vuforia_TrackableBehaviour1779888572.h"
#include "Vuforia_UnityExtensions_Vuforia_TrackableBehaviour4057911311.h"
#include "Vuforia_UnityExtensions_Vuforia_TrackableBehaviour3993660444.h"
#include "Vuforia_UnityExtensions_Vuforia_TrackableSource2832298792.h"
#include "Vuforia_UnityExtensions_Vuforia_Tracker189438242.h"
#include "Vuforia_UnityExtensions_Vuforia_TrackerManager308318605.h"
#include "Vuforia_UnityExtensions_Vuforia_TurnOffAbstractBeh4084926705.h"
#include "Vuforia_UnityExtensions_Vuforia_UserDefinedTargetB3589690572.h"
#include "Vuforia_UnityExtensions_Vuforia_VideoBackgroundAbst395384314.h"
#include "Vuforia_UnityExtensions_Vuforia_VideoBackgroundMan3515346924.h"
#include "Vuforia_UnityExtensions_Vuforia_VirtualButton3703236737.h"
#include "Vuforia_UnityExtensions_Vuforia_VirtualButton_Sens1678924861.h"
#include "Vuforia_UnityExtensions_Vuforia_VirtualButtonAbstr2478279366.h"
#include "Vuforia_UnityExtensions_Vuforia_WebCamARController2804466264.h"
#include "Vuforia_UnityExtensions_Vuforia_WordManager1585193471.h"
#include "Vuforia_UnityExtensions_Vuforia_WordResult1915507197.h"
#include "Vuforia_UnityExtensions_Vuforia_WordTemplateMode1097144495.h"
#include "Vuforia_UnityExtensions_Vuforia_WordAbstractBehavi2878458725.h"
#include "Vuforia_UnityExtensions_Vuforia_WordFilterMode695600879.h"
#include "Vuforia_UnityExtensions_Vuforia_WordList1278495262.h"
#include "Vuforia_UnityExtensions_Vuforia_EyewearCalibration2396922556.h"
#include "Vuforia_UnityExtensions_Vuforia_EyewearUserCalibrat626398268.h"
#include "Vuforia_UnityExtensions_U3CPrivateImplementationDe1486305137.h"
#include "Vuforia_UnityExtensions_U3CPrivateImplementationDet978476007.h"
#include "AssemblyU2DCSharp_U3CModuleU3E3783534214.h"
#include "AssemblyU2DCSharp_ARscript2596852652.h"
#include "AssemblyU2DCSharp_AugmentedScript524146549.h"
#include "AssemblyU2DCSharp_AugmentedScript_U3CGetCoordinate2530575918.h"
#include "AssemblyU2DCSharp_GPS3691620964.h"
#include "AssemblyU2DCSharp_GPS_U3CStartLocationServiceU3Ec__560764166.h"
#include "AssemblyU2DCSharp_PhoneCamera3083977407.h"
#include "AssemblyU2DCSharp_Vuforia_BackgroundPlaneBehaviour2431285219.h"
#include "AssemblyU2DCSharp_Vuforia_CloudRecoBehaviour3077176941.h"
#include "AssemblyU2DCSharp_Vuforia_CylinderTargetBehaviour2091399712.h"



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1900 = { sizeof (VuforiaNativeWrapper_t2645113514), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1901 = { sizeof (VuforiaWrapper_t3750170617), -1, sizeof(VuforiaWrapper_t3750170617_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1901[2] = 
{
	VuforiaWrapper_t3750170617_StaticFields::get_offset_of_sWrapper_0(),
	VuforiaWrapper_t3750170617_StaticFields::get_offset_of_sCamIndependentWrapper_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1902 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1903 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1904 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1905 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1906 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1907 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1908 = { sizeof (ReconstructionAbstractBehaviour_t3509595417), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1908[22] = 
{
	ReconstructionAbstractBehaviour_t3509595417::get_offset_of_mHasInitialized_2(),
	ReconstructionAbstractBehaviour_t3509595417::get_offset_of_mSmartTerrainEventHandlers_3(),
	ReconstructionAbstractBehaviour_t3509595417::get_offset_of_mOnInitialized_4(),
	ReconstructionAbstractBehaviour_t3509595417::get_offset_of_mOnPropCreated_5(),
	ReconstructionAbstractBehaviour_t3509595417::get_offset_of_mOnPropUpdated_6(),
	ReconstructionAbstractBehaviour_t3509595417::get_offset_of_mOnPropDeleted_7(),
	ReconstructionAbstractBehaviour_t3509595417::get_offset_of_mOnSurfaceCreated_8(),
	ReconstructionAbstractBehaviour_t3509595417::get_offset_of_mOnSurfaceUpdated_9(),
	ReconstructionAbstractBehaviour_t3509595417::get_offset_of_mOnSurfaceDeleted_10(),
	ReconstructionAbstractBehaviour_t3509595417::get_offset_of_mInitializedInEditor_11(),
	ReconstructionAbstractBehaviour_t3509595417::get_offset_of_mMaximumExtentEnabled_12(),
	ReconstructionAbstractBehaviour_t3509595417::get_offset_of_mMaximumExtent_13(),
	ReconstructionAbstractBehaviour_t3509595417::get_offset_of_mAutomaticStart_14(),
	ReconstructionAbstractBehaviour_t3509595417::get_offset_of_mNavMeshUpdates_15(),
	ReconstructionAbstractBehaviour_t3509595417::get_offset_of_mNavMeshPadding_16(),
	ReconstructionAbstractBehaviour_t3509595417::get_offset_of_mReconstruction_17(),
	ReconstructionAbstractBehaviour_t3509595417::get_offset_of_mSurfaces_18(),
	ReconstructionAbstractBehaviour_t3509595417::get_offset_of_mActiveSurfaceBehaviours_19(),
	ReconstructionAbstractBehaviour_t3509595417::get_offset_of_mProps_20(),
	ReconstructionAbstractBehaviour_t3509595417::get_offset_of_mActivePropBehaviours_21(),
	ReconstructionAbstractBehaviour_t3509595417::get_offset_of_mPreviouslySetWorldCenterSurfaceTemplate_22(),
	ReconstructionAbstractBehaviour_t3509595417::get_offset_of_mIgnoreNextUpdate_23(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1909 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1910 = { sizeof (PropAbstractBehaviour_t1047177596), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1910[2] = 
{
	PropAbstractBehaviour_t1047177596::get_offset_of_mProp_14(),
	PropAbstractBehaviour_t1047177596::get_offset_of_mBoxColliderToUpdate_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1911 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1912 = { sizeof (SmartTerrainTracker_t1462833936), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1913 = { sizeof (StateManager_t3369465942), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1914 = { sizeof (StateManagerImpl_t3885489748), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1914[9] = 
{
	StateManagerImpl_t3885489748::get_offset_of_mTrackableBehaviours_0(),
	StateManagerImpl_t3885489748::get_offset_of_mAutomaticallyCreatedBehaviours_1(),
	StateManagerImpl_t3885489748::get_offset_of_mBehavioursMarkedForDeletion_2(),
	StateManagerImpl_t3885489748::get_offset_of_mActiveTrackableBehaviours_3(),
	StateManagerImpl_t3885489748::get_offset_of_mWordManager_4(),
	StateManagerImpl_t3885489748::get_offset_of_mVuMarkManager_5(),
	StateManagerImpl_t3885489748::get_offset_of_mDeviceTrackingManager_6(),
	StateManagerImpl_t3885489748::get_offset_of_mCameraPositioningHelper_7(),
	StateManagerImpl_t3885489748::get_offset_of_mExtendedTrackingManager_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1915 = { sizeof (TargetFinderImpl_t1380851697), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1915[4] = 
{
	TargetFinderImpl_t1380851697::get_offset_of_mTargetFinderStatePtr_0(),
	TargetFinderImpl_t1380851697::get_offset_of_mTargetFinderState_1(),
	TargetFinderImpl_t1380851697::get_offset_of_mNewResults_2(),
	TargetFinderImpl_t1380851697::get_offset_of_mImageTargets_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1916 = { sizeof (TargetFinderState_t3807887646)+ sizeof (Il2CppObject), sizeof(TargetFinderState_t3807887646 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1916[4] = 
{
	TargetFinderState_t3807887646::get_offset_of_IsRequesting_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TargetFinderState_t3807887646::get_offset_of_UpdateState_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TargetFinderState_t3807887646::get_offset_of_ResultCount_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TargetFinderState_t3807887646::get_offset_of_unused_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1917 = { sizeof (InternalTargetSearchResult_t2369108641)+ sizeof (Il2CppObject), sizeof(InternalTargetSearchResult_t2369108641 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1917[6] = 
{
	InternalTargetSearchResult_t2369108641::get_offset_of_TargetNamePtr_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	InternalTargetSearchResult_t2369108641::get_offset_of_UniqueTargetIdPtr_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	InternalTargetSearchResult_t2369108641::get_offset_of_MetaDataPtr_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	InternalTargetSearchResult_t2369108641::get_offset_of_TargetSearchResultPtr_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	InternalTargetSearchResult_t2369108641::get_offset_of_TargetSize_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	InternalTargetSearchResult_t2369108641::get_offset_of_TrackingRating_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1918 = { sizeof (TrackableSourceImpl_t2574642394), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1918[1] = 
{
	TrackableSourceImpl_t2574642394::get_offset_of_U3CTrackableSourcePtrU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1919 = { sizeof (TextureRenderer_t3312477626), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1919[3] = 
{
	TextureRenderer_t3312477626::get_offset_of_mTextureBufferCamera_0(),
	TextureRenderer_t3312477626::get_offset_of_mTextureWidth_1(),
	TextureRenderer_t3312477626::get_offset_of_mTextureHeight_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1920 = { sizeof (TrackableImpl_t3421455115), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1920[2] = 
{
	TrackableImpl_t3421455115::get_offset_of_U3CNameU3Ek__BackingField_0(),
	TrackableImpl_t3421455115::get_offset_of_U3CIDU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1921 = { sizeof (TrackerManagerImpl_t381223961), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1921[5] = 
{
	TrackerManagerImpl_t381223961::get_offset_of_mObjectTracker_1(),
	TrackerManagerImpl_t381223961::get_offset_of_mTextTracker_2(),
	TrackerManagerImpl_t381223961::get_offset_of_mSmartTerrainTracker_3(),
	TrackerManagerImpl_t381223961::get_offset_of_mDeviceTracker_4(),
	TrackerManagerImpl_t381223961::get_offset_of_mStateManager_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1922 = { sizeof (VirtualButtonImpl_t2449737797), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1922[6] = 
{
	VirtualButtonImpl_t2449737797::get_offset_of_mName_1(),
	VirtualButtonImpl_t2449737797::get_offset_of_mID_2(),
	VirtualButtonImpl_t2449737797::get_offset_of_mArea_3(),
	VirtualButtonImpl_t2449737797::get_offset_of_mIsEnabled_4(),
	VirtualButtonImpl_t2449737797::get_offset_of_mParentImageTarget_5(),
	VirtualButtonImpl_t2449737797::get_offset_of_mParentDataSet_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1923 = { sizeof (WebCamImpl_t2771725761), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1923[15] = 
{
	WebCamImpl_t2771725761::get_offset_of_mARCameras_0(),
	WebCamImpl_t2771725761::get_offset_of_mOriginalCameraCullMask_1(),
	WebCamImpl_t2771725761::get_offset_of_mWebCamTexture_2(),
	WebCamImpl_t2771725761::get_offset_of_mVideoModeData_3(),
	WebCamImpl_t2771725761::get_offset_of_mVideoTextureInfo_4(),
	WebCamImpl_t2771725761::get_offset_of_mTextureRenderer_5(),
	WebCamImpl_t2771725761::get_offset_of_mBufferReadTexture_6(),
	WebCamImpl_t2771725761::get_offset_of_mReadPixelsRect_7(),
	WebCamImpl_t2771725761::get_offset_of_mWebCamProfile_8(),
	WebCamImpl_t2771725761::get_offset_of_mFlipHorizontally_9(),
	WebCamImpl_t2771725761::get_offset_of_mIsDirty_10(),
	WebCamImpl_t2771725761::get_offset_of_mLastFrameIdx_11(),
	WebCamImpl_t2771725761::get_offset_of_mRenderTextureLayer_12(),
	WebCamImpl_t2771725761::get_offset_of_mWebcamPlaying_13(),
	WebCamImpl_t2771725761::get_offset_of_U3CIsTextureSizeAvailableU3Ek__BackingField_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1924 = { sizeof (WebCamProfile_t3757625748), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1924[1] = 
{
	WebCamProfile_t3757625748::get_offset_of_mProfileCollection_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1925 = { sizeof (ProfileData_t1724666488)+ sizeof (Il2CppObject), sizeof(ProfileData_t1724666488 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1925[3] = 
{
	ProfileData_t1724666488::get_offset_of_RequestedTextureSize_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ProfileData_t1724666488::get_offset_of_ResampledTextureSize_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ProfileData_t1724666488::get_offset_of_RequestedFPS_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1926 = { sizeof (ProfileCollection_t3644865120)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1926[2] = 
{
	ProfileCollection_t3644865120::get_offset_of_DefaultProfile_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ProfileCollection_t3644865120::get_offset_of_Profiles_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1927 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1928 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1929 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1930 = { sizeof (Image_t1391689025), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1931 = { sizeof (PIXEL_FORMAT_t3010530044)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1931[7] = 
{
	PIXEL_FORMAT_t3010530044::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1932 = { sizeof (ImageTargetAbstractBehaviour_t3327552701), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1932[8] = 
{
	ImageTargetAbstractBehaviour_t3327552701::get_offset_of_mAspectRatio_20(),
	ImageTargetAbstractBehaviour_t3327552701::get_offset_of_mImageTargetType_21(),
	ImageTargetAbstractBehaviour_t3327552701::get_offset_of_mWidth_22(),
	ImageTargetAbstractBehaviour_t3327552701::get_offset_of_mHeight_23(),
	ImageTargetAbstractBehaviour_t3327552701::get_offset_of_mImageTarget_24(),
	ImageTargetAbstractBehaviour_t3327552701::get_offset_of_mVirtualButtonBehaviours_25(),
	ImageTargetAbstractBehaviour_t3327552701::get_offset_of_mLastTransformScale_26(),
	ImageTargetAbstractBehaviour_t3327552701::get_offset_of_mLastSize_27(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1933 = { sizeof (ObjectTracker_t1568044035), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1934 = { sizeof (MaskOutAbstractBehaviour_t3489038957), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1934[1] = 
{
	MaskOutAbstractBehaviour_t3489038957::get_offset_of_maskMaterial_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1935 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1936 = { sizeof (MultiTargetAbstractBehaviour_t3616801211), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1936[1] = 
{
	MultiTargetAbstractBehaviour_t3616801211::get_offset_of_mMultiTarget_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1937 = { sizeof (VuforiaUnity_t657456673), -1, sizeof(VuforiaUnity_t657456673_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1937[1] = 
{
	VuforiaUnity_t657456673_StaticFields::get_offset_of_mHoloLensApiAbstraction_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1938 = { sizeof (InitError_t2149396216)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1938[12] = 
{
	InitError_t2149396216::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1939 = { sizeof (VuforiaHint_t3491240575)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1939[4] = 
{
	VuforiaHint_t3491240575::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1940 = { sizeof (StorageType_t3897282321)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1940[4] = 
{
	StorageType_t3897282321::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1941 = { sizeof (VuforiaARController_t4061728485), -1, sizeof(VuforiaARController_t4061728485_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1941[39] = 
{
	VuforiaARController_t4061728485::get_offset_of_CameraDeviceModeSetting_1(),
	VuforiaARController_t4061728485::get_offset_of_MaxSimultaneousImageTargets_2(),
	VuforiaARController_t4061728485::get_offset_of_MaxSimultaneousObjectTargets_3(),
	VuforiaARController_t4061728485::get_offset_of_UseDelayedLoadingObjectTargets_4(),
	VuforiaARController_t4061728485::get_offset_of_CameraDirection_5(),
	VuforiaARController_t4061728485::get_offset_of_MirrorVideoBackground_6(),
	VuforiaARController_t4061728485::get_offset_of_mWorldCenterMode_7(),
	VuforiaARController_t4061728485::get_offset_of_mWorldCenter_8(),
	VuforiaARController_t4061728485::get_offset_of_mTrackerEventHandlers_9(),
	VuforiaARController_t4061728485::get_offset_of_mVideoBgEventHandlers_10(),
	VuforiaARController_t4061728485::get_offset_of_mOnVuforiaInitialized_11(),
	VuforiaARController_t4061728485::get_offset_of_mOnVuforiaStarted_12(),
	VuforiaARController_t4061728485::get_offset_of_mOnVuforiaDeinitialized_13(),
	VuforiaARController_t4061728485::get_offset_of_mOnTrackablesUpdated_14(),
	VuforiaARController_t4061728485::get_offset_of_mRenderOnUpdate_15(),
	VuforiaARController_t4061728485::get_offset_of_mOnPause_16(),
	VuforiaARController_t4061728485::get_offset_of_mPaused_17(),
	VuforiaARController_t4061728485::get_offset_of_mOnBackgroundTextureChanged_18(),
	VuforiaARController_t4061728485::get_offset_of_mStartHasBeenInvoked_19(),
	VuforiaARController_t4061728485::get_offset_of_mHasStarted_20(),
	VuforiaARController_t4061728485::get_offset_of_mBackgroundTextureHasChanged_21(),
	VuforiaARController_t4061728485::get_offset_of_mCameraConfiguration_22(),
	VuforiaARController_t4061728485::get_offset_of_mEyewearBehaviour_23(),
	VuforiaARController_t4061728485::get_offset_of_mVideoBackgroundMgr_24(),
	VuforiaARController_t4061728485::get_offset_of_mCheckStopCamera_25(),
	VuforiaARController_t4061728485::get_offset_of_mClearMaterial_26(),
	VuforiaARController_t4061728485::get_offset_of_mMetalRendering_27(),
	VuforiaARController_t4061728485::get_offset_of_mHasStartedOnce_28(),
	VuforiaARController_t4061728485::get_offset_of_mWasEnabledBeforePause_29(),
	VuforiaARController_t4061728485::get_offset_of_mObjectTrackerWasActiveBeforePause_30(),
	VuforiaARController_t4061728485::get_offset_of_mObjectTrackerWasActiveBeforeDisabling_31(),
	VuforiaARController_t4061728485::get_offset_of_mLastUpdatedFrame_32(),
	VuforiaARController_t4061728485::get_offset_of_mTrackersRequestedToDeinit_33(),
	VuforiaARController_t4061728485::get_offset_of_mMissedToApplyLeftProjectionMatrix_34(),
	VuforiaARController_t4061728485::get_offset_of_mMissedToApplyRightProjectionMatrix_35(),
	VuforiaARController_t4061728485::get_offset_of_mLeftProjectMatrixToApply_36(),
	VuforiaARController_t4061728485::get_offset_of_mRightProjectMatrixToApply_37(),
	VuforiaARController_t4061728485_StaticFields::get_offset_of_mInstance_38(),
	VuforiaARController_t4061728485_StaticFields::get_offset_of_mPadlock_39(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1942 = { sizeof (WorldCenterMode_t3506117492)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1942[5] = 
{
	WorldCenterMode_t3506117492::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1943 = { sizeof (VuforiaMacros_t1884408435)+ sizeof (Il2CppObject), sizeof(VuforiaMacros_t1884408435 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1943[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1944 = { sizeof (VuforiaManager_t2424874861), -1, sizeof(VuforiaManager_t2424874861_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1944[1] = 
{
	VuforiaManager_t2424874861_StaticFields::get_offset_of_sInstance_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1945 = { sizeof (TrackableIdPair_t1329355276)+ sizeof (Il2CppObject), sizeof(TrackableIdPair_t1329355276 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1945[2] = 
{
	TrackableIdPair_t1329355276::get_offset_of_TrackableId_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TrackableIdPair_t1329355276::get_offset_of_ResultId_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1946 = { sizeof (VuforiaRenderer_t2933102835), -1, sizeof(VuforiaRenderer_t2933102835_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1946[1] = 
{
	VuforiaRenderer_t2933102835_StaticFields::get_offset_of_sInstance_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1947 = { sizeof (FpsHint_t1598668988)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1947[5] = 
{
	FpsHint_t1598668988::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1948 = { sizeof (VideoBackgroundReflection_t4106934884)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1948[4] = 
{
	VideoBackgroundReflection_t4106934884::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1949 = { sizeof (VideoBGCfgData_t4137084396)+ sizeof (Il2CppObject), sizeof(VideoBGCfgData_t4137084396 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1949[4] = 
{
	VideoBGCfgData_t4137084396::get_offset_of_position_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	VideoBGCfgData_t4137084396::get_offset_of_size_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	VideoBGCfgData_t4137084396::get_offset_of_enabled_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	VideoBGCfgData_t4137084396::get_offset_of_reflectionInteger_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1950 = { sizeof (Vec2I_t829768013)+ sizeof (Il2CppObject), sizeof(Vec2I_t829768013 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1950[2] = 
{
	Vec2I_t829768013::get_offset_of_x_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Vec2I_t829768013::get_offset_of_y_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1951 = { sizeof (VideoTextureInfo_t2617831468)+ sizeof (Il2CppObject), sizeof(VideoTextureInfo_t2617831468 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1951[2] = 
{
	VideoTextureInfo_t2617831468::get_offset_of_textureSize_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	VideoTextureInfo_t2617831468::get_offset_of_imageSize_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1952 = { sizeof (RendererAPI_t804170727)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1952[5] = 
{
	RendererAPI_t804170727::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1953 = { sizeof (VuforiaRuntimeUtilities_t3083157244), -1, sizeof(VuforiaRuntimeUtilities_t3083157244_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1953[2] = 
{
	VuforiaRuntimeUtilities_t3083157244_StaticFields::get_offset_of_sWebCamUsed_0(),
	VuforiaRuntimeUtilities_t3083157244_StaticFields::get_offset_of_sNativePluginSupport_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1954 = { sizeof (InitializableBool_t1916387570)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1954[4] = 
{
	InitializableBool_t1916387570::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1955 = { sizeof (SurfaceUtilities_t4096327849), -1, sizeof(SurfaceUtilities_t4096327849_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1955[1] = 
{
	SurfaceUtilities_t4096327849_StaticFields::get_offset_of_mScreenOrientation_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1956 = { sizeof (TargetFinder_t1347637805), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1957 = { sizeof (InitState_t4409649)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1957[6] = 
{
	InitState_t4409649::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1958 = { sizeof (UpdateState_t1473252352)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1958[12] = 
{
	UpdateState_t1473252352::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1959 = { sizeof (FilterMode_t3082493643)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1959[3] = 
{
	FilterMode_t3082493643::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1960 = { sizeof (TargetSearchResult_t1958726506)+ sizeof (Il2CppObject), sizeof(TargetSearchResult_t1958726506_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1960[6] = 
{
	TargetSearchResult_t1958726506::get_offset_of_TargetName_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TargetSearchResult_t1958726506::get_offset_of_UniqueTargetId_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TargetSearchResult_t1958726506::get_offset_of_TargetSize_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TargetSearchResult_t1958726506::get_offset_of_MetaData_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TargetSearchResult_t1958726506::get_offset_of_TrackingRating_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TargetSearchResult_t1958726506::get_offset_of_TargetSearchResultPtr_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1961 = { sizeof (TextRecoAbstractBehaviour_t2386081773), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1961[12] = 
{
	TextRecoAbstractBehaviour_t2386081773::get_offset_of_mHasInitialized_2(),
	TextRecoAbstractBehaviour_t2386081773::get_offset_of_mTrackerWasActiveBeforePause_3(),
	TextRecoAbstractBehaviour_t2386081773::get_offset_of_mTrackerWasActiveBeforeDisabling_4(),
	TextRecoAbstractBehaviour_t2386081773::get_offset_of_mWordListFile_5(),
	TextRecoAbstractBehaviour_t2386081773::get_offset_of_mCustomWordListFile_6(),
	TextRecoAbstractBehaviour_t2386081773::get_offset_of_mAdditionalCustomWords_7(),
	TextRecoAbstractBehaviour_t2386081773::get_offset_of_mFilterMode_8(),
	TextRecoAbstractBehaviour_t2386081773::get_offset_of_mFilterListFile_9(),
	TextRecoAbstractBehaviour_t2386081773::get_offset_of_mAdditionalFilterWords_10(),
	TextRecoAbstractBehaviour_t2386081773::get_offset_of_mWordPrefabCreationMode_11(),
	TextRecoAbstractBehaviour_t2386081773::get_offset_of_mMaximumWordInstances_12(),
	TextRecoAbstractBehaviour_t2386081773::get_offset_of_mTextRecoEventHandlers_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1962 = { sizeof (TextTracker_t89845299), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1963 = { sizeof (SimpleTargetData_t3993525265)+ sizeof (Il2CppObject), sizeof(SimpleTargetData_t3993525265 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1963[2] = 
{
	SimpleTargetData_t3993525265::get_offset_of_id_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SimpleTargetData_t3993525265::get_offset_of_unused_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1964 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1965 = { sizeof (TrackableBehaviour_t1779888572), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1965[8] = 
{
	TrackableBehaviour_t1779888572::get_offset_of_U3CTimeStampU3Ek__BackingField_2(),
	TrackableBehaviour_t1779888572::get_offset_of_mTrackableName_3(),
	TrackableBehaviour_t1779888572::get_offset_of_mPreserveChildSize_4(),
	TrackableBehaviour_t1779888572::get_offset_of_mInitializedInEditor_5(),
	TrackableBehaviour_t1779888572::get_offset_of_mPreviousScale_6(),
	TrackableBehaviour_t1779888572::get_offset_of_mStatus_7(),
	TrackableBehaviour_t1779888572::get_offset_of_mTrackable_8(),
	TrackableBehaviour_t1779888572::get_offset_of_mTrackableEventHandlers_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1966 = { sizeof (Status_t4057911311)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1966[7] = 
{
	Status_t4057911311::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1967 = { sizeof (CoordinateSystem_t3993660444)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1967[4] = 
{
	CoordinateSystem_t3993660444::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1968 = { sizeof (TrackableSource_t2832298792), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1969 = { sizeof (Tracker_t189438242), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1969[1] = 
{
	Tracker_t189438242::get_offset_of_U3CIsActiveU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1970 = { sizeof (TrackerManager_t308318605), -1, sizeof(TrackerManager_t308318605_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1970[1] = 
{
	TrackerManager_t308318605_StaticFields::get_offset_of_mInstance_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1971 = { sizeof (TurnOffAbstractBehaviour_t4084926705), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1972 = { sizeof (UserDefinedTargetBuildingAbstractBehaviour_t3589690572), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1972[11] = 
{
	UserDefinedTargetBuildingAbstractBehaviour_t3589690572::get_offset_of_mObjectTracker_2(),
	UserDefinedTargetBuildingAbstractBehaviour_t3589690572::get_offset_of_mLastFrameQuality_3(),
	UserDefinedTargetBuildingAbstractBehaviour_t3589690572::get_offset_of_mCurrentlyScanning_4(),
	UserDefinedTargetBuildingAbstractBehaviour_t3589690572::get_offset_of_mWasScanningBeforeDisable_5(),
	UserDefinedTargetBuildingAbstractBehaviour_t3589690572::get_offset_of_mCurrentlyBuilding_6(),
	UserDefinedTargetBuildingAbstractBehaviour_t3589690572::get_offset_of_mWasBuildingBeforeDisable_7(),
	UserDefinedTargetBuildingAbstractBehaviour_t3589690572::get_offset_of_mOnInitializedCalled_8(),
	UserDefinedTargetBuildingAbstractBehaviour_t3589690572::get_offset_of_mHandlers_9(),
	UserDefinedTargetBuildingAbstractBehaviour_t3589690572::get_offset_of_StopTrackerWhileScanning_10(),
	UserDefinedTargetBuildingAbstractBehaviour_t3589690572::get_offset_of_StartScanningAutomatically_11(),
	UserDefinedTargetBuildingAbstractBehaviour_t3589690572::get_offset_of_StopScanningWhenFinshedBuilding_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1973 = { sizeof (VideoBackgroundAbstractBehaviour_t395384314), -1, sizeof(VideoBackgroundAbstractBehaviour_t395384314_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1973[12] = 
{
	VideoBackgroundAbstractBehaviour_t395384314::get_offset_of_mClearBuffers_2(),
	VideoBackgroundAbstractBehaviour_t395384314::get_offset_of_mSkipStateUpdates_3(),
	VideoBackgroundAbstractBehaviour_t395384314::get_offset_of_mVuforiaARController_4(),
	VideoBackgroundAbstractBehaviour_t395384314::get_offset_of_mCamera_5(),
	VideoBackgroundAbstractBehaviour_t395384314::get_offset_of_mBackgroundBehaviour_6(),
	VideoBackgroundAbstractBehaviour_t395384314::get_offset_of_mStereoDepth_7(),
	VideoBackgroundAbstractBehaviour_t395384314_StaticFields::get_offset_of_mFrameCounter_8(),
	VideoBackgroundAbstractBehaviour_t395384314_StaticFields::get_offset_of_mRenderCounter_9(),
	VideoBackgroundAbstractBehaviour_t395384314::get_offset_of_mResetMatrix_10(),
	VideoBackgroundAbstractBehaviour_t395384314::get_offset_of_mVuforiaFrustumSkew_11(),
	VideoBackgroundAbstractBehaviour_t395384314::get_offset_of_mCenterToEyeAxis_12(),
	VideoBackgroundAbstractBehaviour_t395384314::get_offset_of_mDisabledMeshRenderers_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1974 = { sizeof (VideoBackgroundManager_t3515346924), -1, sizeof(VideoBackgroundManager_t3515346924_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1974[8] = 
{
	VideoBackgroundManager_t3515346924::get_offset_of_mClippingMode_1(),
	VideoBackgroundManager_t3515346924::get_offset_of_mMatteShader_2(),
	VideoBackgroundManager_t3515346924::get_offset_of_mVideoBackgroundEnabled_3(),
	VideoBackgroundManager_t3515346924::get_offset_of_mTexture_4(),
	VideoBackgroundManager_t3515346924::get_offset_of_mVideoBgConfigChanged_5(),
	VideoBackgroundManager_t3515346924::get_offset_of_mNativeTexturePtr_6(),
	VideoBackgroundManager_t3515346924_StaticFields::get_offset_of_mInstance_7(),
	VideoBackgroundManager_t3515346924_StaticFields::get_offset_of_mPadlock_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1975 = { sizeof (VirtualButton_t3703236737), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1975[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1976 = { sizeof (Sensitivity_t1678924861)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1976[4] = 
{
	Sensitivity_t1678924861::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1977 = { sizeof (VirtualButtonAbstractBehaviour_t2478279366), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1977[15] = 
{
	0,
	VirtualButtonAbstractBehaviour_t2478279366::get_offset_of_mName_3(),
	VirtualButtonAbstractBehaviour_t2478279366::get_offset_of_mSensitivity_4(),
	VirtualButtonAbstractBehaviour_t2478279366::get_offset_of_mHasUpdatedPose_5(),
	VirtualButtonAbstractBehaviour_t2478279366::get_offset_of_mPrevTransform_6(),
	VirtualButtonAbstractBehaviour_t2478279366::get_offset_of_mPrevParent_7(),
	VirtualButtonAbstractBehaviour_t2478279366::get_offset_of_mSensitivityDirty_8(),
	VirtualButtonAbstractBehaviour_t2478279366::get_offset_of_mPreviousSensitivity_9(),
	VirtualButtonAbstractBehaviour_t2478279366::get_offset_of_mPreviouslyEnabled_10(),
	VirtualButtonAbstractBehaviour_t2478279366::get_offset_of_mPressed_11(),
	VirtualButtonAbstractBehaviour_t2478279366::get_offset_of_mHandlers_12(),
	VirtualButtonAbstractBehaviour_t2478279366::get_offset_of_mLeftTop_13(),
	VirtualButtonAbstractBehaviour_t2478279366::get_offset_of_mRightBottom_14(),
	VirtualButtonAbstractBehaviour_t2478279366::get_offset_of_mUnregisterOnDestroy_15(),
	VirtualButtonAbstractBehaviour_t2478279366::get_offset_of_mVirtualButton_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1978 = { sizeof (WebCamARController_t2804466264), -1, sizeof(WebCamARController_t2804466264_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1978[6] = 
{
	WebCamARController_t2804466264::get_offset_of_RenderTextureLayer_1(),
	WebCamARController_t2804466264::get_offset_of_mDeviceNameSetInEditor_2(),
	WebCamARController_t2804466264::get_offset_of_mFlipHorizontally_3(),
	WebCamARController_t2804466264::get_offset_of_mWebCamImpl_4(),
	WebCamARController_t2804466264_StaticFields::get_offset_of_mInstance_5(),
	WebCamARController_t2804466264_StaticFields::get_offset_of_mPadlock_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1979 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1980 = { sizeof (WordManager_t1585193471), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1981 = { sizeof (WordResult_t1915507197), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1982 = { sizeof (WordTemplateMode_t1097144495)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1982[3] = 
{
	WordTemplateMode_t1097144495::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1983 = { sizeof (WordAbstractBehaviour_t2878458725), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1983[3] = 
{
	WordAbstractBehaviour_t2878458725::get_offset_of_mMode_10(),
	WordAbstractBehaviour_t2878458725::get_offset_of_mSpecificWord_11(),
	WordAbstractBehaviour_t2878458725::get_offset_of_mWord_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1984 = { sizeof (WordFilterMode_t695600879)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1984[4] = 
{
	WordFilterMode_t695600879::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1985 = { sizeof (WordList_t1278495262), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1986 = { sizeof (EyewearCalibrationProfileManager_t2396922556), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1987 = { sizeof (EyewearUserCalibrator_t626398268), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1988 = { sizeof (U3CPrivateImplementationDetailsU3E_t1486305142), -1, sizeof(U3CPrivateImplementationDetailsU3E_t1486305142_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1988[1] = 
{
	U3CPrivateImplementationDetailsU3E_t1486305142_StaticFields::get_offset_of_U3898C2022A0C02FCE602BF05E1C09BD48301606E5_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1989 = { sizeof (__StaticArrayInitTypeSizeU3D24_t978476007)+ sizeof (Il2CppObject), sizeof(__StaticArrayInitTypeSizeU3D24_t978476007 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1990 = { sizeof (U3CModuleU3E_t3783534221), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1991 = { sizeof (ARscript_t2596852652), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1992 = { sizeof (AugmentedScript_t524146549), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1992[12] = 
{
	AugmentedScript_t524146549::get_offset_of_originalLatitude_2(),
	AugmentedScript_t524146549::get_offset_of_originalLongitude_3(),
	AugmentedScript_t524146549::get_offset_of_currentLongitude_4(),
	AugmentedScript_t524146549::get_offset_of_currentLatitude_5(),
	AugmentedScript_t524146549::get_offset_of_latitude_6(),
	AugmentedScript_t524146549::get_offset_of_longitide_7(),
	AugmentedScript_t524146549::get_offset_of_distanceTextObject_8(),
	AugmentedScript_t524146549::get_offset_of_distance_9(),
	AugmentedScript_t524146549::get_offset_of_setOriginalValues_10(),
	AugmentedScript_t524146549::get_offset_of_targetPosition_11(),
	AugmentedScript_t524146549::get_offset_of_originalPosition_12(),
	AugmentedScript_t524146549::get_offset_of_speed_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1993 = { sizeof (U3CGetCoordinatesU3Ec__Iterator0_t2530575918), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1993[5] = 
{
	U3CGetCoordinatesU3Ec__Iterator0_t2530575918::get_offset_of_U3CmaxWaitU3E__0_0(),
	U3CGetCoordinatesU3Ec__Iterator0_t2530575918::get_offset_of_U24this_1(),
	U3CGetCoordinatesU3Ec__Iterator0_t2530575918::get_offset_of_U24current_2(),
	U3CGetCoordinatesU3Ec__Iterator0_t2530575918::get_offset_of_U24disposing_3(),
	U3CGetCoordinatesU3Ec__Iterator0_t2530575918::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1994 = { sizeof (GPS_t3691620964), -1, sizeof(GPS_t3691620964_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1994[3] = 
{
	GPS_t3691620964_StaticFields::get_offset_of_U3CInstanceU3Ek__BackingField_2(),
	GPS_t3691620964::get_offset_of_latitude_3(),
	GPS_t3691620964::get_offset_of_longitude_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1995 = { sizeof (U3CStartLocationServiceU3Ec__Iterator0_t560764166), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1995[5] = 
{
	U3CStartLocationServiceU3Ec__Iterator0_t560764166::get_offset_of_U3CmaxWaitU3E__0_0(),
	U3CStartLocationServiceU3Ec__Iterator0_t560764166::get_offset_of_U24this_1(),
	U3CStartLocationServiceU3Ec__Iterator0_t560764166::get_offset_of_U24current_2(),
	U3CStartLocationServiceU3Ec__Iterator0_t560764166::get_offset_of_U24disposing_3(),
	U3CStartLocationServiceU3Ec__Iterator0_t560764166::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1996 = { sizeof (PhoneCamera_t3083977407), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1996[5] = 
{
	PhoneCamera_t3083977407::get_offset_of_camAvailable_2(),
	PhoneCamera_t3083977407::get_offset_of_backCam_3(),
	PhoneCamera_t3083977407::get_offset_of_defaultBackground_4(),
	PhoneCamera_t3083977407::get_offset_of_background_5(),
	PhoneCamera_t3083977407::get_offset_of_fit_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1997 = { sizeof (BackgroundPlaneBehaviour_t2431285219), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1998 = { sizeof (CloudRecoBehaviour_t3077176941), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1999 = { sizeof (CylinderTargetBehaviour_t2091399712), -1, 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif

﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// updateGPS
struct updateGPS_t2114334627;

#include "codegen/il2cpp-codegen.h"

// System.Void updateGPS::.ctor()
extern "C"  void updateGPS__ctor_m2845865306 (updateGPS_t2114334627 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void updateGPS::Update()
extern "C"  void updateGPS_Update_m3899016935 (updateGPS_t2114334627 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;

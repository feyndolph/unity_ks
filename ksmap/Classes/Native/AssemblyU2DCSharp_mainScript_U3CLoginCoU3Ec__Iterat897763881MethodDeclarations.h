﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// mainScript/<LoginCo>c__Iterator0
struct U3CLoginCoU3Ec__Iterator0_t897763881;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void mainScript/<LoginCo>c__Iterator0::.ctor()
extern "C"  void U3CLoginCoU3Ec__Iterator0__ctor_m272196884 (U3CLoginCoU3Ec__Iterator0_t897763881 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean mainScript/<LoginCo>c__Iterator0::MoveNext()
extern "C"  bool U3CLoginCoU3Ec__Iterator0_MoveNext_m2211465480 (U3CLoginCoU3Ec__Iterator0_t897763881 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object mainScript/<LoginCo>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CLoginCoU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m292936604 (U3CLoginCoU3Ec__Iterator0_t897763881 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object mainScript/<LoginCo>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CLoginCoU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m3718931284 (U3CLoginCoU3Ec__Iterator0_t897763881 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void mainScript/<LoginCo>c__Iterator0::Dispose()
extern "C"  void U3CLoginCoU3Ec__Iterator0_Dispose_m3527491619 (U3CLoginCoU3Ec__Iterator0_t897763881 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void mainScript/<LoginCo>c__Iterator0::Reset()
extern "C"  void U3CLoginCoU3Ec__Iterator0_Reset_m87423069 (U3CLoginCoU3Ec__Iterator0_t897763881 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;

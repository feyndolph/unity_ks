﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Vuforia_DefaultInitializationErro965510117.h"
#include "AssemblyU2DCSharp_Vuforia_DefaultSmartTerrainEventH870608571.h"
#include "AssemblyU2DCSharp_Vuforia_DefaultTrackableEventHan1082256726.h"
#include "AssemblyU2DCSharp_Vuforia_GLErrorHandler3809113141.h"
#include "AssemblyU2DCSharp_Vuforia_HideExcessAreaBehaviour3495034315.h"
#include "AssemblyU2DCSharp_Vuforia_ImageTargetBehaviour2654589389.h"
#include "AssemblyU2DCSharp_Vuforia_AndroidUnityPlayer852788525.h"
#include "AssemblyU2DCSharp_Vuforia_ComponentFactoryStarterB3249343815.h"
#include "AssemblyU2DCSharp_Vuforia_IOSUnityPlayer3656371703.h"
#include "AssemblyU2DCSharp_Vuforia_VuforiaBehaviourComponen1383853028.h"
#include "AssemblyU2DCSharp_Vuforia_WSAUnityPlayer425981959.h"
#include "AssemblyU2DCSharp_Vuforia_MaskOutBehaviour2994129365.h"
#include "AssemblyU2DCSharp_Vuforia_MultiTargetBehaviour3504654311.h"
#include "AssemblyU2DCSharp_Vuforia_ObjectTargetBehaviour3836044259.h"
#include "AssemblyU2DCSharp_Vuforia_PropBehaviour966064926.h"
#include "AssemblyU2DCSharp_Vuforia_ReconstructionBehaviour4009935945.h"
#include "AssemblyU2DCSharp_Vuforia_ReconstructionFromTarget2111803406.h"
#include "AssemblyU2DCSharp_Vuforia_SurfaceBehaviour2405314212.h"
#include "AssemblyU2DCSharp_Vuforia_TextRecoBehaviour3400239837.h"
#include "AssemblyU2DCSharp_Vuforia_TurnOffBehaviour3058161409.h"
#include "AssemblyU2DCSharp_Vuforia_TurnOffWordBehaviour584991835.h"
#include "AssemblyU2DCSharp_Vuforia_UserDefinedTargetBuildin4184040062.h"
#include "AssemblyU2DCSharp_VRIntegrationHelper556656694.h"
#include "AssemblyU2DCSharp_Vuforia_VideoBackgroundBehaviour3161817952.h"
#include "AssemblyU2DCSharp_Vuforia_VirtualButtonBehaviour2515041812.h"
#include "AssemblyU2DCSharp_Vuforia_VuMarkBehaviour2060629989.h"
#include "AssemblyU2DCSharp_Vuforia_VuforiaBehaviour359035403.h"
#include "AssemblyU2DCSharp_Vuforia_VuforiaConfiguration3823746026.h"
#include "AssemblyU2DCSharp_Vuforia_VuforiaRuntimeInitializa1850075444.h"
#include "AssemblyU2DCSharp_Vuforia_WireframeBehaviour2494532455.h"
#include "AssemblyU2DCSharp_Vuforia_WireframeTrackableEventH1535150527.h"
#include "AssemblyU2DCSharp_Vuforia_WordBehaviour3366478421.h"
#include "AssemblyU2DCSharp_Vuforia_dataTarget2601526595.h"
#include "AssemblyU2DCSharp_disableBtn3218920940.h"
#include "AssemblyU2DCSharp_dropdownController2436896817.h"
#include "AssemblyU2DCSharp_googleAPI1242675179.h"
#include "AssemblyU2DCSharp_googleAPI_mapType4018907404.h"
#include "AssemblyU2DCSharp_googleAPI_U3CGetCoordinatesU3Ec_2822646618.h"
#include "AssemblyU2DCSharp_googleAPI_U3CMapU3Ec__Iterator13582373118.h"
#include "AssemblyU2DCSharp_gpsScript3990414477.h"
#include "AssemblyU2DCSharp_gpsScript_mapType4160084606.h"
#include "AssemblyU2DCSharp_gpsScript_U3CGetCoordinatesU3Ec_3613863740.h"
#include "AssemblyU2DCSharp_gpsScript_U3CMapU3Ec__Iterator12604267004.h"
#include "AssemblyU2DCSharp_mainScript3805801630.h"
#include "AssemblyU2DCSharp_mainScript_U3CLoginCoU3Ec__Iterat897763881.h"
#include "AssemblyU2DCSharp_updateGPS2114334627.h"



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2000 = { sizeof (DefaultInitializationErrorHandler_t965510117), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2000[3] = 
{
	DefaultInitializationErrorHandler_t965510117::get_offset_of_mErrorText_2(),
	DefaultInitializationErrorHandler_t965510117::get_offset_of_mErrorOccurred_3(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2001 = { sizeof (DefaultSmartTerrainEventHandler_t870608571), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2001[3] = 
{
	DefaultSmartTerrainEventHandler_t870608571::get_offset_of_mReconstructionBehaviour_2(),
	DefaultSmartTerrainEventHandler_t870608571::get_offset_of_PropTemplate_3(),
	DefaultSmartTerrainEventHandler_t870608571::get_offset_of_SurfaceTemplate_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2002 = { sizeof (DefaultTrackableEventHandler_t1082256726), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2002[5] = 
{
	DefaultTrackableEventHandler_t1082256726::get_offset_of_TextTargetName_2(),
	DefaultTrackableEventHandler_t1082256726::get_offset_of_ButtonAction_3(),
	DefaultTrackableEventHandler_t1082256726::get_offset_of_TextDescription_4(),
	DefaultTrackableEventHandler_t1082256726::get_offset_of_PanelDescription_5(),
	DefaultTrackableEventHandler_t1082256726::get_offset_of_mTrackableBehaviour_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2003 = { sizeof (GLErrorHandler_t3809113141), -1, sizeof(GLErrorHandler_t3809113141_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2003[3] = 
{
	GLErrorHandler_t3809113141_StaticFields::get_offset_of_mErrorText_2(),
	GLErrorHandler_t3809113141_StaticFields::get_offset_of_mErrorOccurred_3(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2004 = { sizeof (HideExcessAreaBehaviour_t3495034315), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2005 = { sizeof (ImageTargetBehaviour_t2654589389), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2006 = { sizeof (AndroidUnityPlayer_t852788525), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2006[6] = 
{
	0,
	0,
	AndroidUnityPlayer_t852788525::get_offset_of_mScreenOrientation_2(),
	AndroidUnityPlayer_t852788525::get_offset_of_mJavaScreenOrientation_3(),
	AndroidUnityPlayer_t852788525::get_offset_of_mFramesSinceLastOrientationReset_4(),
	AndroidUnityPlayer_t852788525::get_offset_of_mFramesSinceLastJavaOrientationCheck_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2007 = { sizeof (ComponentFactoryStarterBehaviour_t3249343815), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2008 = { sizeof (IOSUnityPlayer_t3656371703), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2008[1] = 
{
	IOSUnityPlayer_t3656371703::get_offset_of_mScreenOrientation_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2009 = { sizeof (VuforiaBehaviourComponentFactory_t1383853028), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2010 = { sizeof (WSAUnityPlayer_t425981959), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2010[1] = 
{
	WSAUnityPlayer_t425981959::get_offset_of_mScreenOrientation_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2011 = { sizeof (MaskOutBehaviour_t2994129365), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2012 = { sizeof (MultiTargetBehaviour_t3504654311), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2013 = { sizeof (ObjectTargetBehaviour_t3836044259), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2014 = { sizeof (PropBehaviour_t966064926), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2015 = { sizeof (ReconstructionBehaviour_t4009935945), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2016 = { sizeof (ReconstructionFromTargetBehaviour_t2111803406), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2017 = { sizeof (SurfaceBehaviour_t2405314212), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2018 = { sizeof (TextRecoBehaviour_t3400239837), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2019 = { sizeof (TurnOffBehaviour_t3058161409), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2020 = { sizeof (TurnOffWordBehaviour_t584991835), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2021 = { sizeof (UserDefinedTargetBuildingBehaviour_t4184040062), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2022 = { sizeof (VRIntegrationHelper_t556656694), -1, sizeof(VRIntegrationHelper_t556656694_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2022[12] = 
{
	VRIntegrationHelper_t556656694_StaticFields::get_offset_of_mLeftCameraMatrixOriginal_2(),
	VRIntegrationHelper_t556656694_StaticFields::get_offset_of_mRightCameraMatrixOriginal_3(),
	VRIntegrationHelper_t556656694_StaticFields::get_offset_of_mLeftCamera_4(),
	VRIntegrationHelper_t556656694_StaticFields::get_offset_of_mRightCamera_5(),
	VRIntegrationHelper_t556656694_StaticFields::get_offset_of_mLeftExcessAreaBehaviour_6(),
	VRIntegrationHelper_t556656694_StaticFields::get_offset_of_mRightExcessAreaBehaviour_7(),
	VRIntegrationHelper_t556656694_StaticFields::get_offset_of_mLeftCameraPixelRect_8(),
	VRIntegrationHelper_t556656694_StaticFields::get_offset_of_mRightCameraPixelRect_9(),
	VRIntegrationHelper_t556656694_StaticFields::get_offset_of_mLeftCameraDataAcquired_10(),
	VRIntegrationHelper_t556656694_StaticFields::get_offset_of_mRightCameraDataAcquired_11(),
	VRIntegrationHelper_t556656694::get_offset_of_IsLeft_12(),
	VRIntegrationHelper_t556656694::get_offset_of_TrackableParent_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2023 = { sizeof (VideoBackgroundBehaviour_t3161817952), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2024 = { sizeof (VirtualButtonBehaviour_t2515041812), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2025 = { sizeof (VuMarkBehaviour_t2060629989), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2026 = { sizeof (VuforiaBehaviour_t359035403), -1, sizeof(VuforiaBehaviour_t359035403_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2026[1] = 
{
	VuforiaBehaviour_t359035403_StaticFields::get_offset_of_mVuforiaBehaviour_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2027 = { sizeof (VuforiaConfiguration_t3823746026), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2028 = { sizeof (VuforiaRuntimeInitialization_t1850075444), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2029 = { sizeof (WireframeBehaviour_t2494532455), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2029[4] = 
{
	WireframeBehaviour_t2494532455::get_offset_of_lineMaterial_2(),
	WireframeBehaviour_t2494532455::get_offset_of_ShowLines_3(),
	WireframeBehaviour_t2494532455::get_offset_of_LineColor_4(),
	WireframeBehaviour_t2494532455::get_offset_of_mLineMaterial_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2030 = { sizeof (WireframeTrackableEventHandler_t1535150527), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2030[1] = 
{
	WireframeTrackableEventHandler_t1535150527::get_offset_of_mTrackableBehaviour_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2031 = { sizeof (WordBehaviour_t3366478421), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2032 = { sizeof (dataTarget_t2601526595), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2032[6] = 
{
	dataTarget_t2601526595::get_offset_of_TextTargetName_2(),
	dataTarget_t2601526595::get_offset_of_TextDescrption_3(),
	dataTarget_t2601526595::get_offset_of_ButtonAction_4(),
	dataTarget_t2601526595::get_offset_of_PanelDescription_5(),
	dataTarget_t2601526595::get_offset_of_soundTarget_6(),
	dataTarget_t2601526595::get_offset_of_clipTarget_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2033 = { sizeof (disableBtn_t3218920940), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2033[3] = 
{
	disableBtn_t3218920940::get_offset_of_myButton_2(),
	disableBtn_t3218920940::get_offset_of_counter_3(),
	disableBtn_t3218920940::get_offset_of_disableImg_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2034 = { sizeof (dropdownController_t2436896817), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2034[4] = 
{
	dropdownController_t2436896817::get_offset_of_names_2(),
	dropdownController_t2436896817::get_offset_of_dropdown_3(),
	dropdownController_t2436896817::get_offset_of_mainImg_4(),
	dropdownController_t2436896817::get_offset_of_ks_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2035 = { sizeof (googleAPI_t1242675179), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2035[15] = 
{
	googleAPI_t1242675179::get_offset_of_img_2(),
	googleAPI_t1242675179::get_offset_of_setOriginalValues_3(),
	googleAPI_t1242675179::get_offset_of_originalLatitude_4(),
	googleAPI_t1242675179::get_offset_of_originalLongitude_5(),
	googleAPI_t1242675179::get_offset_of_currentLongitude_6(),
	googleAPI_t1242675179::get_offset_of_currentLatitude_7(),
	googleAPI_t1242675179::get_offset_of_url_8(),
	googleAPI_t1242675179::get_offset_of_lat_9(),
	googleAPI_t1242675179::get_offset_of_lon_10(),
	googleAPI_t1242675179::get_offset_of_li_11(),
	googleAPI_t1242675179::get_offset_of_zoom_12(),
	googleAPI_t1242675179::get_offset_of_mapWidth_13(),
	googleAPI_t1242675179::get_offset_of_mapHeight_14(),
	googleAPI_t1242675179::get_offset_of_mapSelected_15(),
	googleAPI_t1242675179::get_offset_of_scale_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2036 = { sizeof (mapType_t4018907404)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2036[5] = 
{
	mapType_t4018907404::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2037 = { sizeof (U3CGetCoordinatesU3Ec__Iterator0_t2822646618), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2037[5] = 
{
	U3CGetCoordinatesU3Ec__Iterator0_t2822646618::get_offset_of_U3CmaxWaitU3E__0_0(),
	U3CGetCoordinatesU3Ec__Iterator0_t2822646618::get_offset_of_U24this_1(),
	U3CGetCoordinatesU3Ec__Iterator0_t2822646618::get_offset_of_U24current_2(),
	U3CGetCoordinatesU3Ec__Iterator0_t2822646618::get_offset_of_U24disposing_3(),
	U3CGetCoordinatesU3Ec__Iterator0_t2822646618::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2038 = { sizeof (U3CMapU3Ec__Iterator1_t3582373118), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2038[5] = 
{
	U3CMapU3Ec__Iterator1_t3582373118::get_offset_of_U3CwwwU3E__0_0(),
	U3CMapU3Ec__Iterator1_t3582373118::get_offset_of_U24this_1(),
	U3CMapU3Ec__Iterator1_t3582373118::get_offset_of_U24current_2(),
	U3CMapU3Ec__Iterator1_t3582373118::get_offset_of_U24disposing_3(),
	U3CMapU3Ec__Iterator1_t3582373118::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2039 = { sizeof (gpsScript_t3990414477), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2039[18] = 
{
	gpsScript_t3990414477::get_offset_of_img_2(),
	gpsScript_t3990414477::get_offset_of_myButton_3(),
	gpsScript_t3990414477::get_offset_of_setOriginalValues_4(),
	gpsScript_t3990414477::get_offset_of_originalLatitude_5(),
	gpsScript_t3990414477::get_offset_of_originalLongitude_6(),
	gpsScript_t3990414477::get_offset_of_currentLongitude_7(),
	gpsScript_t3990414477::get_offset_of_currentLatitude_8(),
	gpsScript_t3990414477::get_offset_of_url_9(),
	gpsScript_t3990414477::get_offset_of_lat_10(),
	gpsScript_t3990414477::get_offset_of_lon_11(),
	gpsScript_t3990414477::get_offset_of_li_12(),
	gpsScript_t3990414477::get_offset_of_zoom_13(),
	gpsScript_t3990414477::get_offset_of_mapWidth_14(),
	gpsScript_t3990414477::get_offset_of_mapHeight_15(),
	gpsScript_t3990414477::get_offset_of_mapSelected_16(),
	gpsScript_t3990414477::get_offset_of_scale_17(),
	gpsScript_t3990414477::get_offset_of_latitude_18(),
	gpsScript_t3990414477::get_offset_of_longitide_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2040 = { sizeof (mapType_t4160084606)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2040[5] = 
{
	mapType_t4160084606::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2041 = { sizeof (U3CGetCoordinatesU3Ec__Iterator0_t3613863740), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2041[5] = 
{
	U3CGetCoordinatesU3Ec__Iterator0_t3613863740::get_offset_of_U3CmaxWaitU3E__0_0(),
	U3CGetCoordinatesU3Ec__Iterator0_t3613863740::get_offset_of_U24this_1(),
	U3CGetCoordinatesU3Ec__Iterator0_t3613863740::get_offset_of_U24current_2(),
	U3CGetCoordinatesU3Ec__Iterator0_t3613863740::get_offset_of_U24disposing_3(),
	U3CGetCoordinatesU3Ec__Iterator0_t3613863740::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2042 = { sizeof (U3CMapU3Ec__Iterator1_t2604267004), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2042[5] = 
{
	U3CMapU3Ec__Iterator1_t2604267004::get_offset_of_U3CwwwU3E__0_0(),
	U3CMapU3Ec__Iterator1_t2604267004::get_offset_of_U24this_1(),
	U3CMapU3Ec__Iterator1_t2604267004::get_offset_of_U24current_2(),
	U3CMapU3Ec__Iterator1_t2604267004::get_offset_of_U24disposing_3(),
	U3CMapU3Ec__Iterator1_t2604267004::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2043 = { sizeof (mainScript_t3805801630), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2043[5] = 
{
	mainScript_t3805801630::get_offset_of_As_2(),
	mainScript_t3805801630::get_offset_of_loginPanel_3(),
	mainScript_t3805801630::get_offset_of_IdInputField_4(),
	mainScript_t3805801630::get_offset_of_PassInputField_5(),
	mainScript_t3805801630::get_offset_of_counter_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2044 = { sizeof (U3CLoginCoU3Ec__Iterator0_t897763881), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2044[4] = 
{
	U3CLoginCoU3Ec__Iterator0_t897763881::get_offset_of_U24this_0(),
	U3CLoginCoU3Ec__Iterator0_t897763881::get_offset_of_U24current_1(),
	U3CLoginCoU3Ec__Iterator0_t897763881::get_offset_of_U24disposing_2(),
	U3CLoginCoU3Ec__Iterator0_t897763881::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2045 = { sizeof (updateGPS_t2114334627), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2045[1] = 
{
	updateGPS_t2114334627::get_offset_of_coordinates_2(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif

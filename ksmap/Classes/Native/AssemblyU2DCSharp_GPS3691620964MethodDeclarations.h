﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GPS
struct GPS_t3691620964;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GPS3691620964.h"

// System.Void GPS::.ctor()
extern "C"  void GPS__ctor_m2952425523 (GPS_t3691620964 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GPS::set_Instance(GPS)
extern "C"  void GPS_set_Instance_m2330582003 (Il2CppObject * __this /* static, unused */, GPS_t3691620964 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GPS GPS::get_Instance()
extern "C"  GPS_t3691620964 * GPS_get_Instance_m2061733234 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GPS::Start()
extern "C"  void GPS_Start_m3768757951 (GPS_t3691620964 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator GPS::StartLocationService()
extern "C"  Il2CppObject * GPS_StartLocationService_m734871645 (GPS_t3691620964 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;

﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.Text
struct Text_t356221433;
// UnityEngine.GameObject
struct GameObject_t1756533147;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AugmentedScript
struct  AugmentedScript_t524146549  : public MonoBehaviour_t1158329972
{
public:
	// System.Single AugmentedScript::originalLatitude
	float ___originalLatitude_2;
	// System.Single AugmentedScript::originalLongitude
	float ___originalLongitude_3;
	// System.Single AugmentedScript::currentLongitude
	float ___currentLongitude_4;
	// System.Single AugmentedScript::currentLatitude
	float ___currentLatitude_5;
	// UnityEngine.UI.Text AugmentedScript::latitude
	Text_t356221433 * ___latitude_6;
	// UnityEngine.UI.Text AugmentedScript::longitide
	Text_t356221433 * ___longitide_7;
	// UnityEngine.GameObject AugmentedScript::distanceTextObject
	GameObject_t1756533147 * ___distanceTextObject_8;
	// System.Double AugmentedScript::distance
	double ___distance_9;
	// System.Boolean AugmentedScript::setOriginalValues
	bool ___setOriginalValues_10;
	// UnityEngine.Vector3 AugmentedScript::targetPosition
	Vector3_t2243707580  ___targetPosition_11;
	// UnityEngine.Vector3 AugmentedScript::originalPosition
	Vector3_t2243707580  ___originalPosition_12;
	// System.Single AugmentedScript::speed
	float ___speed_13;

public:
	inline static int32_t get_offset_of_originalLatitude_2() { return static_cast<int32_t>(offsetof(AugmentedScript_t524146549, ___originalLatitude_2)); }
	inline float get_originalLatitude_2() const { return ___originalLatitude_2; }
	inline float* get_address_of_originalLatitude_2() { return &___originalLatitude_2; }
	inline void set_originalLatitude_2(float value)
	{
		___originalLatitude_2 = value;
	}

	inline static int32_t get_offset_of_originalLongitude_3() { return static_cast<int32_t>(offsetof(AugmentedScript_t524146549, ___originalLongitude_3)); }
	inline float get_originalLongitude_3() const { return ___originalLongitude_3; }
	inline float* get_address_of_originalLongitude_3() { return &___originalLongitude_3; }
	inline void set_originalLongitude_3(float value)
	{
		___originalLongitude_3 = value;
	}

	inline static int32_t get_offset_of_currentLongitude_4() { return static_cast<int32_t>(offsetof(AugmentedScript_t524146549, ___currentLongitude_4)); }
	inline float get_currentLongitude_4() const { return ___currentLongitude_4; }
	inline float* get_address_of_currentLongitude_4() { return &___currentLongitude_4; }
	inline void set_currentLongitude_4(float value)
	{
		___currentLongitude_4 = value;
	}

	inline static int32_t get_offset_of_currentLatitude_5() { return static_cast<int32_t>(offsetof(AugmentedScript_t524146549, ___currentLatitude_5)); }
	inline float get_currentLatitude_5() const { return ___currentLatitude_5; }
	inline float* get_address_of_currentLatitude_5() { return &___currentLatitude_5; }
	inline void set_currentLatitude_5(float value)
	{
		___currentLatitude_5 = value;
	}

	inline static int32_t get_offset_of_latitude_6() { return static_cast<int32_t>(offsetof(AugmentedScript_t524146549, ___latitude_6)); }
	inline Text_t356221433 * get_latitude_6() const { return ___latitude_6; }
	inline Text_t356221433 ** get_address_of_latitude_6() { return &___latitude_6; }
	inline void set_latitude_6(Text_t356221433 * value)
	{
		___latitude_6 = value;
		Il2CppCodeGenWriteBarrier(&___latitude_6, value);
	}

	inline static int32_t get_offset_of_longitide_7() { return static_cast<int32_t>(offsetof(AugmentedScript_t524146549, ___longitide_7)); }
	inline Text_t356221433 * get_longitide_7() const { return ___longitide_7; }
	inline Text_t356221433 ** get_address_of_longitide_7() { return &___longitide_7; }
	inline void set_longitide_7(Text_t356221433 * value)
	{
		___longitide_7 = value;
		Il2CppCodeGenWriteBarrier(&___longitide_7, value);
	}

	inline static int32_t get_offset_of_distanceTextObject_8() { return static_cast<int32_t>(offsetof(AugmentedScript_t524146549, ___distanceTextObject_8)); }
	inline GameObject_t1756533147 * get_distanceTextObject_8() const { return ___distanceTextObject_8; }
	inline GameObject_t1756533147 ** get_address_of_distanceTextObject_8() { return &___distanceTextObject_8; }
	inline void set_distanceTextObject_8(GameObject_t1756533147 * value)
	{
		___distanceTextObject_8 = value;
		Il2CppCodeGenWriteBarrier(&___distanceTextObject_8, value);
	}

	inline static int32_t get_offset_of_distance_9() { return static_cast<int32_t>(offsetof(AugmentedScript_t524146549, ___distance_9)); }
	inline double get_distance_9() const { return ___distance_9; }
	inline double* get_address_of_distance_9() { return &___distance_9; }
	inline void set_distance_9(double value)
	{
		___distance_9 = value;
	}

	inline static int32_t get_offset_of_setOriginalValues_10() { return static_cast<int32_t>(offsetof(AugmentedScript_t524146549, ___setOriginalValues_10)); }
	inline bool get_setOriginalValues_10() const { return ___setOriginalValues_10; }
	inline bool* get_address_of_setOriginalValues_10() { return &___setOriginalValues_10; }
	inline void set_setOriginalValues_10(bool value)
	{
		___setOriginalValues_10 = value;
	}

	inline static int32_t get_offset_of_targetPosition_11() { return static_cast<int32_t>(offsetof(AugmentedScript_t524146549, ___targetPosition_11)); }
	inline Vector3_t2243707580  get_targetPosition_11() const { return ___targetPosition_11; }
	inline Vector3_t2243707580 * get_address_of_targetPosition_11() { return &___targetPosition_11; }
	inline void set_targetPosition_11(Vector3_t2243707580  value)
	{
		___targetPosition_11 = value;
	}

	inline static int32_t get_offset_of_originalPosition_12() { return static_cast<int32_t>(offsetof(AugmentedScript_t524146549, ___originalPosition_12)); }
	inline Vector3_t2243707580  get_originalPosition_12() const { return ___originalPosition_12; }
	inline Vector3_t2243707580 * get_address_of_originalPosition_12() { return &___originalPosition_12; }
	inline void set_originalPosition_12(Vector3_t2243707580  value)
	{
		___originalPosition_12 = value;
	}

	inline static int32_t get_offset_of_speed_13() { return static_cast<int32_t>(offsetof(AugmentedScript_t524146549, ___speed_13)); }
	inline float get_speed_13() const { return ___speed_13; }
	inline float* get_address_of_speed_13() { return &___speed_13; }
	inline void set_speed_13(float value)
	{
		___speed_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif

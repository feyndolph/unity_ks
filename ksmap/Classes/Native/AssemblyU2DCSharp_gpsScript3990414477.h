﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.RawImage
struct RawImage_t2749640213;
// UnityEngine.UI.Button
struct Button_t2872111280;
// System.String
struct String_t;
// UnityEngine.UI.Text
struct Text_t356221433;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_LocationInfo1364725149.h"
#include "AssemblyU2DCSharp_gpsScript_mapType4160084606.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// gpsScript
struct  gpsScript_t3990414477  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.UI.RawImage gpsScript::img
	RawImage_t2749640213 * ___img_2;
	// UnityEngine.UI.Button gpsScript::myButton
	Button_t2872111280 * ___myButton_3;
	// System.Boolean gpsScript::setOriginalValues
	bool ___setOriginalValues_4;
	// System.Single gpsScript::originalLatitude
	float ___originalLatitude_5;
	// System.Single gpsScript::originalLongitude
	float ___originalLongitude_6;
	// System.Single gpsScript::currentLongitude
	float ___currentLongitude_7;
	// System.Single gpsScript::currentLatitude
	float ___currentLatitude_8;
	// System.String gpsScript::url
	String_t* ___url_9;
	// System.Single gpsScript::lat
	float ___lat_10;
	// System.Single gpsScript::lon
	float ___lon_11;
	// UnityEngine.LocationInfo gpsScript::li
	LocationInfo_t1364725149  ___li_12;
	// System.Int32 gpsScript::zoom
	int32_t ___zoom_13;
	// System.Int32 gpsScript::mapWidth
	int32_t ___mapWidth_14;
	// System.Int32 gpsScript::mapHeight
	int32_t ___mapHeight_15;
	// gpsScript/mapType gpsScript::mapSelected
	int32_t ___mapSelected_16;
	// System.Int32 gpsScript::scale
	int32_t ___scale_17;
	// UnityEngine.UI.Text gpsScript::latitude
	Text_t356221433 * ___latitude_18;
	// UnityEngine.UI.Text gpsScript::longitide
	Text_t356221433 * ___longitide_19;

public:
	inline static int32_t get_offset_of_img_2() { return static_cast<int32_t>(offsetof(gpsScript_t3990414477, ___img_2)); }
	inline RawImage_t2749640213 * get_img_2() const { return ___img_2; }
	inline RawImage_t2749640213 ** get_address_of_img_2() { return &___img_2; }
	inline void set_img_2(RawImage_t2749640213 * value)
	{
		___img_2 = value;
		Il2CppCodeGenWriteBarrier(&___img_2, value);
	}

	inline static int32_t get_offset_of_myButton_3() { return static_cast<int32_t>(offsetof(gpsScript_t3990414477, ___myButton_3)); }
	inline Button_t2872111280 * get_myButton_3() const { return ___myButton_3; }
	inline Button_t2872111280 ** get_address_of_myButton_3() { return &___myButton_3; }
	inline void set_myButton_3(Button_t2872111280 * value)
	{
		___myButton_3 = value;
		Il2CppCodeGenWriteBarrier(&___myButton_3, value);
	}

	inline static int32_t get_offset_of_setOriginalValues_4() { return static_cast<int32_t>(offsetof(gpsScript_t3990414477, ___setOriginalValues_4)); }
	inline bool get_setOriginalValues_4() const { return ___setOriginalValues_4; }
	inline bool* get_address_of_setOriginalValues_4() { return &___setOriginalValues_4; }
	inline void set_setOriginalValues_4(bool value)
	{
		___setOriginalValues_4 = value;
	}

	inline static int32_t get_offset_of_originalLatitude_5() { return static_cast<int32_t>(offsetof(gpsScript_t3990414477, ___originalLatitude_5)); }
	inline float get_originalLatitude_5() const { return ___originalLatitude_5; }
	inline float* get_address_of_originalLatitude_5() { return &___originalLatitude_5; }
	inline void set_originalLatitude_5(float value)
	{
		___originalLatitude_5 = value;
	}

	inline static int32_t get_offset_of_originalLongitude_6() { return static_cast<int32_t>(offsetof(gpsScript_t3990414477, ___originalLongitude_6)); }
	inline float get_originalLongitude_6() const { return ___originalLongitude_6; }
	inline float* get_address_of_originalLongitude_6() { return &___originalLongitude_6; }
	inline void set_originalLongitude_6(float value)
	{
		___originalLongitude_6 = value;
	}

	inline static int32_t get_offset_of_currentLongitude_7() { return static_cast<int32_t>(offsetof(gpsScript_t3990414477, ___currentLongitude_7)); }
	inline float get_currentLongitude_7() const { return ___currentLongitude_7; }
	inline float* get_address_of_currentLongitude_7() { return &___currentLongitude_7; }
	inline void set_currentLongitude_7(float value)
	{
		___currentLongitude_7 = value;
	}

	inline static int32_t get_offset_of_currentLatitude_8() { return static_cast<int32_t>(offsetof(gpsScript_t3990414477, ___currentLatitude_8)); }
	inline float get_currentLatitude_8() const { return ___currentLatitude_8; }
	inline float* get_address_of_currentLatitude_8() { return &___currentLatitude_8; }
	inline void set_currentLatitude_8(float value)
	{
		___currentLatitude_8 = value;
	}

	inline static int32_t get_offset_of_url_9() { return static_cast<int32_t>(offsetof(gpsScript_t3990414477, ___url_9)); }
	inline String_t* get_url_9() const { return ___url_9; }
	inline String_t** get_address_of_url_9() { return &___url_9; }
	inline void set_url_9(String_t* value)
	{
		___url_9 = value;
		Il2CppCodeGenWriteBarrier(&___url_9, value);
	}

	inline static int32_t get_offset_of_lat_10() { return static_cast<int32_t>(offsetof(gpsScript_t3990414477, ___lat_10)); }
	inline float get_lat_10() const { return ___lat_10; }
	inline float* get_address_of_lat_10() { return &___lat_10; }
	inline void set_lat_10(float value)
	{
		___lat_10 = value;
	}

	inline static int32_t get_offset_of_lon_11() { return static_cast<int32_t>(offsetof(gpsScript_t3990414477, ___lon_11)); }
	inline float get_lon_11() const { return ___lon_11; }
	inline float* get_address_of_lon_11() { return &___lon_11; }
	inline void set_lon_11(float value)
	{
		___lon_11 = value;
	}

	inline static int32_t get_offset_of_li_12() { return static_cast<int32_t>(offsetof(gpsScript_t3990414477, ___li_12)); }
	inline LocationInfo_t1364725149  get_li_12() const { return ___li_12; }
	inline LocationInfo_t1364725149 * get_address_of_li_12() { return &___li_12; }
	inline void set_li_12(LocationInfo_t1364725149  value)
	{
		___li_12 = value;
	}

	inline static int32_t get_offset_of_zoom_13() { return static_cast<int32_t>(offsetof(gpsScript_t3990414477, ___zoom_13)); }
	inline int32_t get_zoom_13() const { return ___zoom_13; }
	inline int32_t* get_address_of_zoom_13() { return &___zoom_13; }
	inline void set_zoom_13(int32_t value)
	{
		___zoom_13 = value;
	}

	inline static int32_t get_offset_of_mapWidth_14() { return static_cast<int32_t>(offsetof(gpsScript_t3990414477, ___mapWidth_14)); }
	inline int32_t get_mapWidth_14() const { return ___mapWidth_14; }
	inline int32_t* get_address_of_mapWidth_14() { return &___mapWidth_14; }
	inline void set_mapWidth_14(int32_t value)
	{
		___mapWidth_14 = value;
	}

	inline static int32_t get_offset_of_mapHeight_15() { return static_cast<int32_t>(offsetof(gpsScript_t3990414477, ___mapHeight_15)); }
	inline int32_t get_mapHeight_15() const { return ___mapHeight_15; }
	inline int32_t* get_address_of_mapHeight_15() { return &___mapHeight_15; }
	inline void set_mapHeight_15(int32_t value)
	{
		___mapHeight_15 = value;
	}

	inline static int32_t get_offset_of_mapSelected_16() { return static_cast<int32_t>(offsetof(gpsScript_t3990414477, ___mapSelected_16)); }
	inline int32_t get_mapSelected_16() const { return ___mapSelected_16; }
	inline int32_t* get_address_of_mapSelected_16() { return &___mapSelected_16; }
	inline void set_mapSelected_16(int32_t value)
	{
		___mapSelected_16 = value;
	}

	inline static int32_t get_offset_of_scale_17() { return static_cast<int32_t>(offsetof(gpsScript_t3990414477, ___scale_17)); }
	inline int32_t get_scale_17() const { return ___scale_17; }
	inline int32_t* get_address_of_scale_17() { return &___scale_17; }
	inline void set_scale_17(int32_t value)
	{
		___scale_17 = value;
	}

	inline static int32_t get_offset_of_latitude_18() { return static_cast<int32_t>(offsetof(gpsScript_t3990414477, ___latitude_18)); }
	inline Text_t356221433 * get_latitude_18() const { return ___latitude_18; }
	inline Text_t356221433 ** get_address_of_latitude_18() { return &___latitude_18; }
	inline void set_latitude_18(Text_t356221433 * value)
	{
		___latitude_18 = value;
		Il2CppCodeGenWriteBarrier(&___latitude_18, value);
	}

	inline static int32_t get_offset_of_longitide_19() { return static_cast<int32_t>(offsetof(gpsScript_t3990414477, ___longitide_19)); }
	inline Text_t356221433 * get_longitide_19() const { return ___longitide_19; }
	inline Text_t356221433 ** get_address_of_longitide_19() { return &___longitide_19; }
	inline void set_longitide_19(Text_t356221433 * value)
	{
		___longitide_19 = value;
		Il2CppCodeGenWriteBarrier(&___longitide_19, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif

﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// gpsScript/<Map>c__Iterator1
struct U3CMapU3Ec__Iterator1_t2604267004;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void gpsScript/<Map>c__Iterator1::.ctor()
extern "C"  void U3CMapU3Ec__Iterator1__ctor_m2083391181 (U3CMapU3Ec__Iterator1_t2604267004 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean gpsScript/<Map>c__Iterator1::MoveNext()
extern "C"  bool U3CMapU3Ec__Iterator1_MoveNext_m1114040075 (U3CMapU3Ec__Iterator1_t2604267004 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object gpsScript/<Map>c__Iterator1::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CMapU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3008973335 (U3CMapU3Ec__Iterator1_t2604267004 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object gpsScript/<Map>c__Iterator1::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CMapU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m1169876975 (U3CMapU3Ec__Iterator1_t2604267004 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void gpsScript/<Map>c__Iterator1::Dispose()
extern "C"  void U3CMapU3Ec__Iterator1_Dispose_m2715341286 (U3CMapU3Ec__Iterator1_t2604267004 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void gpsScript/<Map>c__Iterator1::Reset()
extern "C"  void U3CMapU3Ec__Iterator1_Reset_m2659776104 (U3CMapU3Ec__Iterator1_t2604267004 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;

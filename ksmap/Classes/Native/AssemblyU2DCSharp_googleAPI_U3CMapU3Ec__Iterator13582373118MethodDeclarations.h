﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// googleAPI/<Map>c__Iterator1
struct U3CMapU3Ec__Iterator1_t3582373118;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void googleAPI/<Map>c__Iterator1::.ctor()
extern "C"  void U3CMapU3Ec__Iterator1__ctor_m4090020851 (U3CMapU3Ec__Iterator1_t3582373118 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean googleAPI/<Map>c__Iterator1::MoveNext()
extern "C"  bool U3CMapU3Ec__Iterator1_MoveNext_m199291449 (U3CMapU3Ec__Iterator1_t3582373118 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object googleAPI/<Map>c__Iterator1::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CMapU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1699431225 (U3CMapU3Ec__Iterator1_t3582373118 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object googleAPI/<Map>c__Iterator1::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CMapU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m1132611681 (U3CMapU3Ec__Iterator1_t3582373118 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void googleAPI/<Map>c__Iterator1::Dispose()
extern "C"  void U3CMapU3Ec__Iterator1_Dispose_m3305938256 (U3CMapU3Ec__Iterator1_t3582373118 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void googleAPI/<Map>c__Iterator1::Reset()
extern "C"  void U3CMapU3Ec__Iterator1_Reset_m3986352318 (U3CMapU3Ec__Iterator1_t3582373118 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;

﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// googleAPI
struct googleAPI_t1242675179;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "codegen/il2cpp-codegen.h"

// System.Void googleAPI::.ctor()
extern "C"  void googleAPI__ctor_m2437213868 (googleAPI_t1242675179 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void googleAPI::Start()
extern "C"  void googleAPI_Start_m2581432624 (googleAPI_t1242675179 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void googleAPI::Update()
extern "C"  void googleAPI_Update_m1295155031 (googleAPI_t1242675179 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator googleAPI::GetCoordinates()
extern "C"  Il2CppObject * googleAPI_GetCoordinates_m3064111355 (googleAPI_t1242675179 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator googleAPI::Map()
extern "C"  Il2CppObject * googleAPI_Map_m3180544518 (googleAPI_t1242675179 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;

﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// gpsScript
struct gpsScript_t3990414477;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "codegen/il2cpp-codegen.h"

// System.Void gpsScript::.ctor()
extern "C"  void gpsScript__ctor_m245958002 (gpsScript_t3990414477 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator gpsScript::GetCoordinates()
extern "C"  Il2CppObject * gpsScript_GetCoordinates_m3580238553 (gpsScript_t3990414477 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator gpsScript::Map()
extern "C"  Il2CppObject * gpsScript_Map_m4012126484 (gpsScript_t3990414477 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void gpsScript::Start()
extern "C"  void gpsScript_Start_m3757275986 (gpsScript_t3990414477 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void gpsScript::startGoogleMap()
extern "C"  void gpsScript_startGoogleMap_m3709874505 (gpsScript_t3990414477 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void gpsScript::Update()
extern "C"  void gpsScript_Update_m3536376245 (gpsScript_t3990414477 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
